#!/usr/bin/env python

import sys
import argparse
import string
import collections   #defaultdict
from collections import Counter
from operator import add
from math import log
import itertools

class Classifier(object):
    def __init__(self, name):
        self.classifier_name = name
        
    def classify (self, dna_seq, profile_table, kmer_length, score_cutoff_thresh):
    	if (self.score_by_profile (dna_seq, profile_table, kmer_length) > score_cutoff_thresh):
    		return 1
    	else:
    		return 0

    def score_by_profile (self, dna_seq, profile_table, kmer_length):
    	best_score = 0.0   # holds the score for the most likely motif to have matched motif profile table
    	for i in xrange (len(dna_seq) - kmer_length + 1):
    		kmer = dna_seq[i:i+kmer_length]
    		score = 1.0
    		for pos in xrange(len(kmer)):
    			dna_base = kmer[pos]
    			score *= profile_table[pos][dna_base]
    		if (best_score < score):
    			best_score = score
    	return best_score


##############################
## NOT USED
##############################
    # def hamming_distance (self, seq1, seq2):
    #     assert len(seq1) == len(seq2)
    #     hamm_dist = 0
    #     for i in xrange(len(seq1)):
    #         if (seq1[i] != seq2[i]):
    #             hamm_dist += 1
    #     return hamm_dist

    # # checks if a certain motif occurs in the dna sequence with hamming distance <= hamm_dist
    # def motif_in_dna (self, dna_seq, motif_seq, hamm_dist):
    #     kmer_length = len(motif_seq)
    #     for i in xrange (len(dna_seq) - kmer_length + 1):
    #         kmer = dna_seq[i:i+kmer_length]
    #         if (self.hamming_distance (kmer, motif_seq) <= hamm_dist):
    #             return True
    #     return False

    # # returns the lowest hamming distance a motif is from any parts of the dna sequence
    # def score_hamming_distance (self, dna_seq, motif_seq):
    #     kmer_length = len(motif_seq)
    #     lowest_hamm_dist = len(motif_seq)

    #     for i in xrange (len(dna_seq) - kmer_length + 1):
    #         kmer = dna_seq[i:i+kmer_length]
    #         hamm_dist = self.hamming_distance (kmer, motif_seq)
    #         if (hamm_dist < lowest_hamm_dist):
    #             lowest_hamm_dist = hamm_dist
    #     return lowest_hamm_dist

    # # generates the most likely motif based on profile probabilities
    # def motif_from_profile (self, profile_table):
    #     motif = ''
    #     for i in xrange(len(profile_table)):
    #         max_prob = max (profile_table[i].values())
    #         for key, val in profile_table[i].iteritems():
    #             if val == max_prob:
    #                 motif += key
    #     return motif




