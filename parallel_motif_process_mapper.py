#!/usr/bin/env python

import sys
import argparse
import string
import time

from process_motif_lib import * #assuming same directory

if __name__ == "__main__":

	parser = argparse.ArgumentParser(description = 'Bioinformatics')
	parser.add_argument('--input_path', metavar='path', type=str, help='Input file path')
	parser.add_argument('--kmer_length', metavar='int', type=int, help='Kmer length of interest')
	parser.add_argument('--command', metavar='string', type=str, help='Enter what to do to input')
	# parser.add_argument('--hamm_bound', metavar='int', type=int, help='Hamming distance allowed from pattern')
	args = parser.parse_args()

	textfile = open(args.input_path,'r')
	text = textfile.read()
	my_motif = Motif (text)

	if (args.command == 'greedy_motif_selection'):
		input_list = my_motif.seq.strip().split('\n')
		# start_time = time.time ()
		best_motif_list = my_motif.motif_selection_greedy (input_list, args.kmer_length, True)
		most_likely_profile = my_motif.build_profile (best_motif_list, args.kmer_length, True)
		# end_time = time.time ()
		# elapsed_time = end_time - start_time
		# print 'Elapsed Time : ' + str(elapsed_time)

		bases = ['A','C','T','G']
		for base in bases:
			profile_output = base + ' '
			for base_prob_at_position in most_likely_profile:
				profile_output += str(base_prob_at_position[base]) + ' '
			print profile_output

		# test code for best motif list output
		# for motif in best_motif_list:
		# 	print motif
