#!/usr/bin/env python

import sys
import argparse
import string
import time
import random
import numpy

from classify_by_motif_lib import * #assuming same directory

def generate_random_motif (kmer_length):
    bases = ['A', 'C', 'T', 'G']
    random_motif = ''
    for i in xrange (kmer_length):
        random_index = random.randint(0,3)
        random_motif += bases[random_index]
    return random_motif

if __name__ == "__main__":

	parser = argparse.ArgumentParser(description = 'Bioinformatics')
	parser.add_argument('--input_path', metavar='path', type=str, help='Input file path')
	parser.add_argument('--profile_path', metavar='path', type=str, help='Motif profile probability file path')
	parser.add_argument('--kmer_length', metavar='int', type=int, help='Kmer length of interest')
	args = parser.parse_args()

	textfile = open(args.input_path,'r')
	seqs = textfile.read()
	textfile.close()

	my_classifier = Classifier ('motif_match')
	textfile = open(args.profile_path, 'r')
	profile_raw = textfile.read()

	seq_list = seqs.strip().split('\n')
	profile_by_base_list = profile_raw.strip().split('\n')

	# loading best motifs profile table from file into a list of dictionaries of floats
	profile_table = [{} for i in xrange(args.kmer_length)]
	for base_profile_raw in profile_by_base_list:
		temp_list = base_profile_raw.strip().split(' ')   # temp_list should contain something like: 'A' '0.22' '0.11' '0.67' for 3-mer
		base = temp_list[0]
		for j in xrange (1,len(temp_list)):   # length of temp_list should be 1 + kmer_length , since the base string is attached at beginning
			profile_at_position = profile_table[j-1]
			profile_at_position[base] = float(temp_list[j])
	# print profile_table

	# used when input is all positive
	positive_score_list = []
	for k in xrange(len(seq_list)):
		rand_seq_score = my_classifier.score_by_profile (seq_list[k], profile_table, args.kmer_length)
		positive_score_list.append (rand_seq_score)
		# print 'sequence ' + str(k) + ' ' + str (rand_seq_score)
	pos_array = numpy.array(positive_score_list)
	pos_mean = pos_array.mean()
	pos_std = pos_array.std()
	print 'positive mean: ' + str(pos_mean) + ' standard deviation: ' + str(pos_std)

	# checking random sequence best scores versus the training seq scores (see above) to find classification threshold
	negative_score_list = []
	for w in xrange(25):
		random_seq = generate_random_motif (len(seq_list[0]))
		rand_seq_score = my_classifier.score_by_profile (random_seq, profile_table, args.kmer_length)
		negative_score_list.append (rand_seq_score)
		# print 'sequence ' + str(k) + ' ' + str (rand_seq_score)
	neg_array = numpy.array(negative_score_list)
	neg_mean = neg_array.mean()
	neg_std = neg_array.std()
	print 'mean: ' + str(neg_mean) + ' standard deviation: ' + str(neg_std)

