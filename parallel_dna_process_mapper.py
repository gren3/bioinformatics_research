#!/usr/bin/env python

import sys
import argparse
import string
import time

from process_dna_lib import * #assuming same directory

if __name__ == "__main__":

	parser = argparse.ArgumentParser(description = 'Bioinformatics python practice')
	parser.add_argument('--input_path', metavar='path', type=str, help='Input file path')
	parser.add_argument('--kmer_length', metavar='int', type=int, help='Kmer length of interest')
	parser.add_argument('--command', metavar='string', type=str, help='Enter what to do to input')
	parser.add_argument('--hamm_bound', metavar='int', type=int, help='Hamming distance allowed from pattern')
	args = parser.parse_args()

	textfile = open(args.input_path,'r')
	text = textfile.read()
	my_dna = DNA (text)

	if (args.command == 'most_freq_kmer_w_mismatch'):
		kmer_dict = my_dna.build_kmer_dict_w_mismatch_genome (my_dna.seq, args.kmer_length, args.hamm_bound)
		for kmer in kmer_dict:
			print kmer + ' ' + str(kmer_dict[kmer])

		