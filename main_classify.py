#!/usr/bin/env python

import sys
import argparse
import string
import time
import random
import numpy

from classify_by_motif_lib import * #assuming same directory

if __name__ == "__main__":

	parser = argparse.ArgumentParser(description = 'Bioinformatics')
	parser.add_argument('--input_path', metavar='path', type=str, help='Input file path')
	parser.add_argument('--profile_path', metavar='path', type=str, help='Motif profile probability file path')
	parser.add_argument('--kmer_length', metavar='int', type=int, help='Kmer length of interest')
	parser.add_argument('--label_path', metavar='path', type=str, help='Labels for test sequences file path')
	args = parser.parse_args()

	textfile = open(args.input_path,'r')
	seqs = textfile.read()
	textfile.close()

	textfile = open(args.label_path, 'r')
	correct_labels = textfile.read().strip().split('\n')
	textfile.close()

	my_classifier = Classifier ('motif_match')
	textfile = open(args.profile_path, 'r')
	profile_raw = textfile.read()

	seq_list = seqs.strip().split('\n')
	profile_by_base_list = profile_raw.strip().split('\n')

	# loading best motifs profile table from file into a list of dictionaries of floats
	profile_table = [{} for i in xrange(args.kmer_length)]
	for base_profile_raw in profile_by_base_list:
		temp_list = base_profile_raw.strip().split(' ')   # temp_list should contain something like: 'A' '0.22' '0.11' '0.67' for 3-mer
		base = temp_list[0]
		for j in xrange (1,len(temp_list)):   # length of temp_list should be 1 + kmer_length , since the base string is attached at beginning
			profile_at_position = profile_table[j-1]
			profile_at_position[base] = float(temp_list[j])
	# print profile_table

	thresh = 0.0001
	num_correct = 0
	# classification
	for k in xrange(len(seq_list)):
		rand_seq_score = my_classifier.score_by_profile (seq_list[k], profile_table, args.kmer_length)
		print 'sequence ' + str(k) + ' ' + str (rand_seq_score)
		label = my_classifier.classify (seq_list[k], profile_table, args.kmer_length, thresh)
		if (label == int(correct_labels[k])):
			num_correct += 1
	print 'Number of enhancers correctly labeled: ' + str(num_correct)

