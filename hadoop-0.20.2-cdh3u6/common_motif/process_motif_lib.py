#!/usr/bin/env python

import sys
import argparse
import string
import collections   #defaultdict
from collections import Counter
from operator import add
from math import log
import itertools
import random

class Motif(object):
    def __init__(self, string_sequence):
        self.seq = string_sequence.strip()
        
    def hamming_distance (self, seq1, seq2):
        assert len(seq1) == len(seq2)
        hamm_dist = 0
        for i in xrange(len(seq1)):
            if (seq1[i] != seq2[i]):
                hamm_dist += 1
        return hamm_dist

    # generates all patterns within a certain hamming distance away from given sequence
    def build_kmer_dict_w_mismatch (self, dna_seq, hamm_dist):
        mismatch_combos = [''.join(combo) for combo in itertools.product('ACTG', repeat=hamm_dist)]
        mismatch_locations_list = list(itertools.product(range(len(dna_seq)), repeat=hamm_dist))

        mismatch_locations_list_nodup = []
        # saving list items without duplicated tuple elements
        for l in mismatch_locations_list:   # l is a tuple
          duplicated = [x for x, y in collections.Counter(l).items() if y > 1]
          if (len(duplicated) == 0):
            mismatch_locations_list_nodup.append(l)

        kmer_w_mismatch_dict = collections.defaultdict(int)
        for i in xrange(len(mismatch_locations_list_nodup)):  # sequence of list index numbers
          for j in xrange(len(mismatch_combos)):  # sequence of base pair combos
            kmer = list (dna_seq)
            mismatch_locations = mismatch_locations_list_nodup[i]
            for k in xrange(len(mismatch_locations)):  # tuple
              kmer[mismatch_locations[k]] = mismatch_combos[j][k]
            kmer = ''.join(kmer)
            kmer_w_mismatch_dict[kmer] = 1

        return kmer_w_mismatch_dict

    # checks if a certain motif occurs in the dna sequence with hamming distance <= hamm_dist
    # def motif_in_dna (self, dna_seq, motif_seq, hamm_dist):
    #     kmer_length = len(motif_seq)
    #     for i in xrange (len(dna_seq) - kmer_length + 1):
    #         kmer = dna_seq[i:i+kmer_length]
    #         if (self.hamming_distance (kmer, motif_seq) <= hamm_dist):
    #             return True
    #     return False

    # generates a list of motifs with hamming distance away from dna sequence
    def generate_mismatch_motif (self, dna_seq, kmer_length, hamm_dist):
        mismatch_motifs = []

        for j in xrange(len(dna_seq) - kmer_length + 1):
            for k in xrange(1,hamm_dist+1):
                kmer_w_mismatch_dict = self.build_kmer_dict_w_mismatch (dna_seq[j:j+kmer_length], k)
                mismatch_motifs.extend (kmer_w_mismatch_dict.keys())
        return mismatch_motifs

    def common_motif_naive (self, dna_list, kmer_length, hamm_dist):
        all_mismatch_motifs = []

        # generates all motifs within hamming distance <= hamm_dist
        for i in xrange (len(dna_list)):
            dna_seq = dna_list[i]
            all_mismatch_motifs.extend (self.generate_mismatch_motif (dna_seq, kmer_length, hamm_dist))

        # checks if motif occurs within all dna sequences
        common_mismatch_motifs = []
        all_mismatch_motifs = list(set(all_mismatch_motifs))   # removing duplicates
        for motif in all_mismatch_motifs:
            motif_in_all = True
            for dna_seq in dna_list:
                if (self.motif_in_dna (dna_seq, motif, hamm_dist) == False):
                    motif_in_all = False
                    break
            if (motif_in_all):
                common_mismatch_motifs.append (motif)

        return list (set (common_mismatch_motifs))

    # returns the lowest hamming distance a motif is from any parts of the dna sequence
    # def score_hamming_distance (self, dna_seq, motif_seq):
    #     kmer_length = len(motif_seq)
    #     lowest_hamm_dist = len(motif_seq)

    #     for i in xrange (len(dna_seq) - kmer_length + 1):
    #         kmer = dna_seq[i:i+kmer_length]
    #         hamm_dist = self.hamming_distance (kmer, motif_seq)
    #         if (hamm_dist < lowest_hamm_dist):
    #             lowest_hamm_dist = hamm_dist
    #     return lowest_hamm_dist

    # finds the most probable kmer in dna sequence according to profile probability
    # profile table is a list of dictionaries of probabilities for each base at each position in kmer
    def motif_by_profile (self, profile_table, dna_seq, kmer_length):
        highest_consensus_probability = -1.0
        best_profile_motif = ''

        for i in xrange (len(dna_seq) - kmer_length + 1):
            kmer = dna_seq[i:i+kmer_length]
            motif_probability = 1.0
            for j in xrange(len(kmer)):
                probability_dict = profile_table[j]   # returns {'A':#, 'T':#, 'C':#, 'G':#}
                motif_probability *= float(probability_dict[kmer[j]])
            if (motif_probability > highest_consensus_probability):
                highest_consensus_probability = motif_probability
                best_profile_motif = kmer
        return best_profile_motif

    # returns a dna base profile from the sequence list given, table returned is a list of dictionaries for each base position
    def build_profile (self, motif_list, motif_length, pseudocount_padding):
        for dna in motif_list:
            assert len(dna) ==  motif_length

        profile_table = []
        for i in xrange (motif_length):
            if (pseudocount_padding):
                profile = {'A':1.0, 'C':1.0, 'T':1.0, 'G':1.0}                
            else:
                profile = {'A':0.0, 'C':0.0, 'T':0.0, 'G':0.0}
            for dna in motif_list:
                profile[dna[i]] += 1.0
            total_count = sum(profile.values())
            profile = {base:float(count)/float(total_count) for base,count in profile.iteritems()}   # count divide by total count
            profile_table.append (profile)
        return profile_table

    # attempts to quantify entropy/variation within motif list
    def score_motif_list (self, motif_list):
        profile_table = self.build_profile (motif_list, len(motif_list[0]), True)
        score = 1.0
        for i in xrange(len(profile_table)):
            prob_sum = 0.0
            for base,prob in profile_table[i].iteritems():
                if (prob > 0):  # avoids log(0)
                    prob_sum += prob*log(prob,2)
            score *= -prob_sum
        return score

    # TODO need to return a list of best motifs when ties happen
    def motif_selection_greedy (self, dna_list, kmer_length, pseudocount_padding):
        best_motif_list = []

        # initializing motif list to 1st kmer in each dna string
        for dna in dna_list:
            best_motif_list.append (dna[0:kmer_length])

        start_string = dna_list[0]
        best_motif_list_score = self.score_motif_list(best_motif_list)   # initialize score to be score for initial best motif list
        for i in xrange (len(start_string) - kmer_length + 1):  # for each kmer motif in 1st dna string
            motif_list = [start_string[i:i+kmer_length]]
            for j in xrange (1, len(dna_list)):   # from the second sequence onward
                profile_table = self.build_profile (motif_list, kmer_length, pseudocount_padding)
                best_motif = self.motif_by_profile (profile_table, dna_list[j], kmer_length)
                motif_list.append (best_motif)
            motif_list_score = self.score_motif_list(motif_list)
            if (best_motif_list_score > motif_list_score):
                best_motif_list = motif_list
                best_motif_list_score = motif_list_score
        return best_motif_list

