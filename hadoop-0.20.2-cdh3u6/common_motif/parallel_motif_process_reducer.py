#!/usr/bin/env python

import sys
import argparse
import string
import time

cur_base = ''
cur_base_profile_accumulator = []
occurrence_count = 0

# assuming input key, values are sorted by keys and values are a sequence of probabilities at 
# which the key appears at certain location of motif
for line in sys.stdin:
    line = line.strip()
    line_input_list = line.split(' ')
    base = line_input_list[0]
    base_profile = line_input_list[1:]
    if (base != cur_base):
    	output_str = cur_base + ' '
    	for i in xrange(len(cur_base_profile_accumulator)):
    		output_str += str(float(cur_base_profile_accumulator[i])/float(occurrence_count)) + ' '
    	print output_str
    	cur_base = base
    	cur_base_profile_accumulator = base_profile
    	occurrence_count = 1
    else:
    	cur_base_profile_accumulator = [float(x) + float(y) for x, y in zip(cur_base_profile_accumulator, base_profile)]   # sum two list of floats
    	occurrence_count += 1

output_str = cur_base + ' '
for i in xrange(len(cur_base_profile_accumulator)):
    output_str += str(float(cur_base_profile_accumulator[i])/float(occurrence_count)) + ' '
print output_str

