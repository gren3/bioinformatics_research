#!/usr/bin/env python

import sys
from math import log
import itertools

class Motif(object):
    def __init__(self, string_sequence):
        self.seq = string_sequence.strip()
        
    # finds the most probable kmer in dna sequence according to profile probability
    # profile table is a list of dictionaries of probabilities for each base at each position in kmer
    def motif_by_profile (self, profile_table, dna_seq, kmer_length):
        highest_consensus_probability = -1.0
        best_profile_motif = ''

        for i in xrange (len(dna_seq) - kmer_length + 1):
            kmer = dna_seq[i:i+kmer_length]
            motif_probability = 1.0
            for j in xrange(len(kmer)):
                probability_dict = profile_table[j]   # returns {'A':#, 'T':#, 'C':#, 'G':#}
                motif_probability *= float(probability_dict[kmer[j]])
            if (motif_probability > highest_consensus_probability):
                highest_consensus_probability = motif_probability
                best_profile_motif = kmer
        return best_profile_motif

    # returns a dna base profile from the sequence list given, table returned is a list of dictionaries for each base position
    def build_profile (self, motif_list, motif_length, pseudocount_padding):
        for dna in motif_list:
            assert len(dna) ==  motif_length

        profile_table = []
        for i in xrange (motif_length):
            if (pseudocount_padding):
                profile = {'A':1.0, 'C':1.0, 'T':1.0, 'G':1.0}                
            else:
                profile = {'A':0.0, 'C':0.0, 'T':0.0, 'G':0.0}
            for dna in motif_list:
                profile[dna[i]] += 1.0
            total_count = sum(profile.values())
            # map-reduce gives a syntax error even when this line runs fine on own computer
            # profile = {base:float(count)/float(total_count) for base,count in profile.iteritems()}   # count divide by total count

            for base,count in profile.iteritems():
                profile[base] = float(count)/float(total_count)
            profile_table.append (profile)
        return profile_table

    # attempts to quantify entropy/variation within motif list
    def score_motif_list (self, motif_list):
        profile_table = self.build_profile (motif_list, len(motif_list[0]), True)
        score = 1.0
        for i in xrange(len(profile_table)):
            prob_sum = 0.0
            for base,prob in profile_table[i].iteritems():
                if (prob > 0):  # avoids log(0)
                    prob_sum += prob*log(prob,2)
            score *= -prob_sum
        return score

    # TODO need to return a list of best motifs when ties happen
    def motif_selection_greedy (self, dna_list, kmer_length, pseudocount_padding):
        best_motif_list = []

        # initializing motif list to 1st kmer in each dna string
        for dna in dna_list:
            best_motif_list.append (dna[0:kmer_length])

        start_string = dna_list[0]
        best_motif_list_score = self.score_motif_list(best_motif_list)   # initialize score to be score for initial best motif list
        for i in xrange (len(start_string) - kmer_length + 1):  # for each kmer motif in 1st dna string
            motif_list = [start_string[i:i+kmer_length]]
            for j in xrange (1, len(dna_list)):   # from the second sequence onward
                profile_table = self.build_profile (motif_list, kmer_length, pseudocount_padding)
                best_motif = self.motif_by_profile (profile_table, dna_list[j], kmer_length)
                motif_list.append (best_motif)
            motif_list_score = self.score_motif_list(motif_list)
            if (best_motif_list_score > motif_list_score):
                best_motif_list = motif_list
                best_motif_list_score = motif_list_score
        return best_motif_list

my_motif = Motif ('')
kmer_length = 12

input_list = []
for line in sys.stdin:
	input_list.append(line.strip())

best_motif_list = my_motif.motif_selection_greedy (input_list, kmer_length, True)
most_likely_profile = my_motif.build_profile (best_motif_list, kmer_length, True)

bases = ['A','C','T','G']
for base in bases:
	profile_output = base + ' '
	for base_prob_at_position in most_likely_profile:
		profile_output += str(base_prob_at_position[base]) + ' '
	print profile_output


