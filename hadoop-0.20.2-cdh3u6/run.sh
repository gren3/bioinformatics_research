#!/usr/bin/env bash

# command for running hadoop streaming mode in shell, finding most frequent kmer with mismatch in a single dna sequence
# python most_freq_kmer/preprocess_dna.py --input_path 'most_freq_kmer/data.txt' --kmer_length 10 --num_mappers 5 >mapper_input.txt 
# ./bin/hadoop jar contrib/streaming/hadoop-streaming-0.20.2-cdh3u6.jar -mapper most_freq_kmer/parallel_dna_process_mapper_nargs.py -reducer most_freq_kmer/parallel_dna_process_reducer.py -input most_freq_kmer/mapper_input.txt -output most_freq_kmer/output

./bin/hadoop jar contrib/streaming/hadoop-streaming-0.20.2-cdh3u6.jar -mapper common_motif/parallel_motif_process_mapper_nargs.py -reducer common_motif/parallel_motif_process_reducer.py -input ../motif_mapper_input/* -output common_motif/output

