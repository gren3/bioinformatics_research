#!/usr/bin/env python

import sys
import argparse
import string
import time

cur_kmer = ''   # for storing kmer string currently being added
count = 0  # holds the sum of all occurrences from each mapper for the current kmer
max_count = 0
most_freq_kmer = []
for line in sys.stdin:
    line = line.strip()
    kmer, occurrence = line.split(' ')
    if (cur_kmer != kmer):
    	if (count > max_count):
    		max_count = count
    		most_freq_kmer = [cur_kmer]
    	elif (count == max_count):
    		most_freq_kmer.append(cur_kmer)   # if more than 1 kmer has max number of occurrences, save them in a list
    	cur_kmer = kmer
    	count = int(occurrence)
    else:
    	count += int(occurrence)

for kmer in most_freq_kmer:
    print '%s\t%s' % (kmer,max_count)

