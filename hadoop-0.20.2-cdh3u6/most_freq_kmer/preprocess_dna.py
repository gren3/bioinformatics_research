#!/usr/bin/env python

import sys
import argparse
import string
import time

if __name__ == "__main__":

	parser = argparse.ArgumentParser(description = 'Bioinformatics')
	parser.add_argument('--input_path', metavar='path', type=str, help='Input file path')
	parser.add_argument('--kmer_length', metavar='int', type=int, help='Kmer length of interest')
	parser.add_argument('--num_mappers', metavar='int', type=int, help='Max number of mappers available/desired')
	args = parser.parse_args()

	textfile = open(args.input_path,'r')
	whole_dna_seq = textfile.read().strip()   # assuming only a single line of dna sequence

	dna_length_per_mapper = len(whole_dna_seq) / args.num_mappers   # distribute nearly equal chunks of dna sequence to each mapper

	for i in xrange (args.num_mappers):
		if (i == 0):
			mini_dna_seq = whole_dna_seq[i * dna_length_per_mapper : i * dna_length_per_mapper + dna_length_per_mapper]
		elif (i == args.num_mappers - 1):   # for the last mapper, give all remaining sequence, since python division rounds down
			mini_dna_seq = whole_dna_seq[i * dna_length_per_mapper - (args.kmer_length - 1):]
		else:   # starting index contains kmer length - 1 suffix from the previous sequence to simulates continuous dna sequence
			mini_dna_seq = whole_dna_seq[i * dna_length_per_mapper - (args.kmer_length - 1) : i * dna_length_per_mapper + dna_length_per_mapper]
		print mini_dna_seq

