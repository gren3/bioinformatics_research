#!/usr/bin/env python

import sys
import argparse
import string
import collections   #defaultdict
from collections import Counter
from operator import add
import itertools

class DNA(object):
    def __init__(self, bp_string):
        self.seq = bp_string.strip()

    # building dictionary of kmer occurrence counts
    def build_kmer_dict (self, seq, kmer_length):
    	kmer_dict = collections.defaultdict(int)
    	for i in xrange (len(seq) - kmer_length + 1):
    		kmer_dict[seq[i:i+kmer_length]] += 1
    	return kmer_dict

    def reverse_complement (self,dna_seq):
        complement = string.maketrans('ACTG','TGAC')
        return string.translate(dna_seq, complement)[::-1]

    def hamming_distance (self, seq1, seq2):
        assert len(seq1) == len(seq2)
        hamm_dist = 0
        for i in xrange(len(seq1)):
            if (seq1[i] != seq2[i]):
                hamm_dist += 1

        return hamm_dist

    # generates all patterns within a certain hamming distance away from given sequence
    def build_kmer_dict_w_mismatch (self, dna_seq, hamm_dist):
        mismatch_combos = [''.join(combo) for combo in itertools.product('ACTG', repeat=hamm_dist)]
        # print mismatch_combos
        mismatch_locations_list = list(itertools.product(range(len(dna_seq)), repeat=hamm_dist))

        mismatch_locations_list_nodup = []
        # saving list items without duplicated tuple elements
        for l in mismatch_locations_list:   # l is a tuple
          duplicated = [x for x, y in collections.Counter(l).items() if y > 1]
          if (len(duplicated) == 0):
            mismatch_locations_list_nodup.append(l)
        # print mismatch_locations_list_nodup

        kmer_w_mismatch_dict = collections.defaultdict(int)
        for i in xrange(len(mismatch_locations_list_nodup)):  # sequence of list index numbers
          for j in xrange(len(mismatch_combos)):  # sequence of base pair combos
            kmer = list (dna_seq)
            mismatch_locations = mismatch_locations_list_nodup[i]
            for k in xrange(len(mismatch_locations)):  # tuple
              kmer[mismatch_locations[k]] = mismatch_combos[j][k]
            kmer = ''.join(kmer)
            kmer_w_mismatch_dict[kmer] = 1

        return kmer_w_mismatch_dict

    def build_kmer_dict_w_mismatch_genome (self, dna_genome, kmer_length, hamm_dist):
        kmer_dict = self.build_kmer_dict (dna_genome, kmer_length)  # values are the number of occurrences of kmer
        kmer_w_mismatch_dict = {}
        for kmer in kmer_dict:
            occurrences = kmer_dict[kmer]
            # goes through all hamming distances <= hamm_dist
            temp_mismatch_dict = {}
            for dist in xrange(hamm_dist+1):
                temp_mismatch_dict.update(self.build_kmer_dict_w_mismatch(kmer,dist))
            for key in temp_mismatch_dict:
                temp_mismatch_dict[key] = temp_mismatch_dict[key] * occurrences
            temp = [kmer_w_mismatch_dict, temp_mismatch_dict]
            kmer_w_mismatch_dict = reduce(add, (Counter(dict(x)) for x in temp))
        return kmer_w_mismatch_dict

    # used for debugging
    # def find_common_kmer_w_mismatch (self, dna_genome, kmer_length, hamm_dist):
    #     kmer_dict = self.build_kmer_dict_w_mismatch_genome (dna_genome, kmer_length, hamm_dist)

    #     max_count = 0
    #     for key in kmer_dict:
    #         if (kmer_dict[key] > max_count):
    #             max_count = kmer_dict[key]

    #     max_count_kmers = []
    #     for key in kmer_dict:
    #         # print key + " : " + str(kmer_dict[key])
    #         if (kmer_dict[key] == max_count):
    #             max_count_kmers.append (key)
    #     return max_count_kmers

