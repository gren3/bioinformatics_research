#!/usr/bin/env python

import sys
import string
import time
from collections import Counter

from process_dna_lib import * #assuming same directory

my_dna = DNA ('')
dna_seq = ''
kmer_length = 10
hamming_distance = 2
kmer_dict = {}

for line in sys.stdin:
    dna_seq = line.strip()
    temp_kmer_dict = my_dna.build_kmer_dict_w_mismatch_genome (dna_seq, kmer_length, hamming_distance)
    temp = [kmer_dict, temp_kmer_dict]
    kmer_dict = reduce(add, (Counter(dict(x)) for x in temp))

for kmer in kmer_dict:
	print kmer + ' ' + str(kmer_dict[kmer])

