
import java.io.*;
import java.util.*;
import java.lang.*;

public class ProcessDNA{
	public static void main(String[] args){
		String input_seq = "";
		String input_seq2 = "";

		try {
			BufferedReader br = new BufferedReader (new FileReader ("data.txt"));
		    StringBuilder sb = new StringBuilder ();
		    String line = br.readLine ();
		    input_seq = line;
			input_seq2 = br.readLine ();

		    // while (line != null) {
		    //     sb.append (line);
		    //     sb.append ('\n');
		    //     line = br.readLine ();
		    // }
		    // input_seq = sb.toString ();
		}catch (Exception e){
			System.err.println("check for null input");	
			System.exit (0);
		}

		String genome = "ACGTTGCATGTCGCATGATGCATGAGAGCT";
		int kmer_length = 4;
		int hamming_distance = 1;
		List<String> most_freq_seq = find_most_freq_kmer_w_mismatch (genome, kmer_length, hamming_distance);
		for (String seq : most_freq_seq) {
			System.out.print (seq + " ");
		}

		// test code for mismatch kmer generation
		// String kmer = "ACTG";
		// int kmer_length = 4;
		// int hamming_distance = 2;
		// HashMap<String, Integer> mismatch_kmer_hash = build_kmer_hash_w_mismatch (kmer, kmer_length, hamming_distance);
		// System.out.println (mismatch_kmer_hash.size());

		// System.out.println (hamming_distance(input_seq, input_seq2));
		// System.out.println (reverse_complement(input_seq));
		// System.out.println (count_dna(input_seq));
		// System.out.println (dna_to_rna(input_seq));
		// System.out.println (rna_to_protein(input_seq));
	}

	public static List<String> find_most_freq_kmer_w_mismatch (String genome, int kmer_length, int hamming_distance){
		HashMap<String, Integer> mismatch_hash = build_kmer_hash_w_mismatch_genome (genome, kmer_length, hamming_distance);
		List<String> most_freq_list = new ArrayList<String> ();

		// iterate to find max value, found easier way
		// int max_count = 0;
		// for (Map.Entry<String, Integer> mismatch : mismatch_hash.entrySet()){
		// 	Integer mismatch_occurrence = (Integer) mismatch.getValue();
		//     if (mismatch_occurrence.intValue() > max_count){
		//         max_count = mismatch_occurrence;
		//     }
		// }

		int max_count = Collections.max(mismatch_hash.values());
		// iterate to retrieve keys with max value
		for (Map.Entry<String, Integer> mismatch : mismatch_hash.entrySet()){
			int mismatch_occurrence = mismatch.getValue().intValue();
		    if (mismatch_occurrence == max_count){
		        most_freq_list.add (mismatch.getKey());
		    }
		}
		return most_freq_list;
	}

	public static HashMap<String, Integer> build_kmer_hash_w_mismatch_genome (String genome, int kmer_length, int hamming_distance){
		HashMap<String, Integer> kmer_hash = build_kmer_hash (genome, kmer_length);
		HashMap<String, Integer> kmer_w_mismatch_hash_final = new HashMap<String, Integer> ();

		// code adapted from http://stackoverflow.com/questions/1066589/java-iterate-through-hashmap
	    Iterator it = kmer_hash.entrySet().iterator();
	    // iterate through each kmer and occurrences
	    while (it.hasNext()) {
			HashMap<String, Integer> kmer_w_mismatch_hash = new HashMap<String, Integer> ();
	        Map.Entry kmer_pairs = (Map.Entry)it.next();
	        String kmer_seq = (String) kmer_pairs.getKey();
	        Integer occurrences = (Integer) kmer_pairs.getValue();
	        it.remove(); // avoids a ConcurrentModificationException
	       	for (int i = 0; i <= hamming_distance; i++){
	    		kmer_w_mismatch_hash.putAll (build_kmer_hash_w_mismatch (kmer_seq, kmer_length, hamming_distance));
	    	}

			Iterator it2 = kmer_w_mismatch_hash.entrySet().iterator();
		    // iterate through all mismatches and set its value to occurrences of the kmer
		    while (it2.hasNext()) {
		        Map.Entry mismatch_pairs = (Map.Entry)it2.next();
		        String mismatch_seq = (String) mismatch_pairs.getKey();
		        kmer_w_mismatch_hash.put (mismatch_seq, occurrences.intValue());
		        // it2.remove(); // avoids a ConcurrentModificationException
		    }
		    // merging current mismatch hashmap with accumulated hashmap
		    kmer_w_mismatch_hash_final = reduceMaps (kmer_w_mismatch_hash_final, kmer_w_mismatch_hash);
	    }

	    // testing code
		// Iterator it3 = kmer_w_mismatch_hash_final.entrySet().iterator();
	 //    while (it3.hasNext()) {
	 //        Map.Entry mismatch_pairs = (Map.Entry)it3.next();
	 //        String mismatch_seq = (String) mismatch_pairs.getKey();
	 //        System.out.println (mismatch_seq);
	 //    }

		return kmer_w_mismatch_hash_final;
	}

	public static HashMap<String,Integer> build_kmer_hash_w_mismatch (String kmer, int kmer_length, int hamming_distance){
	    List<List<String>> mismatch_dna_list = new ArrayList<List<String>>();
	    List<List<String>> mismatch_locations_list = new ArrayList<List<String>>();

	    // generates a list of list of mismatch locations at which base substitutions occur
	    for (int j = 0; j < hamming_distance; j++){
		    ArrayList<String> indices_list = new ArrayList<String>();
		    for (int k = 0; k < kmer_length; k++){
		    	indices_list.add (Integer.toString(k));
		    }
		    mismatch_locations_list.add (indices_list);
	    }

	    // generates a list of list of dna bases for which the cartesian product is taken
	    for (int i = 0; i < kmer_length; i++){
		    mismatch_dna_list.add (new ArrayList<String>(Arrays.asList("A", "C", "T", "G")));
	    }

	    String[][] mismatch_combos_dna = allUniqueCombinations (mismatch_dna_list);
	    String[][] mismatch_combos_location = allUniqueCombinations (mismatch_locations_list);

	    List<List<Integer>> locations_nodups_list = new ArrayList<List<Integer>>();
	    // iterate through cartesian products and remove ones where locations duplicate
	    for (int j = 0; j < mismatch_combos_location.length; j++){
	    	String mismatch_str = "";
	    	List<Integer> locations = new ArrayList<Integer>();

			for (int k = 0; k < mismatch_combos_location[j].length; k++){
				locations.add(Integer.parseInt(mismatch_combos_location[j][k]));
			    mismatch_str = mismatch_str + mismatch_combos_location[j][k] + " ";
			}

			Integer[] locations_arr = locations.toArray(new Integer[locations.size()]);
			if (!check_for_duplicates(locations_arr)){   // checks if array contains duplicated indices
				locations_nodups_list.add (locations);
				// System.out.println (mismatch_str);
			}
	    }

	    HashMap<String,Integer> mismatch_kmer_hash = new HashMap<String,Integer>();
	    // iterate through list of mismatch locations and save mismatch dna into kmer hash
	    for (int i = 0; i < locations_nodups_list.size(); i++){
	    	List<Integer> locations = locations_nodups_list.get(i);  // each locations is of length hamming distance
	    	for (int j = 0; j < mismatch_combos_dna.length; j++){
				StringBuilder mismatch_string = new StringBuilder(kmer);
	    		for (int k = 0; k < locations.size(); k++){
	    			mismatch_string.setCharAt(locations.get(k).intValue(), mismatch_combos_dna[j][k].charAt(0));
				}
		    	// System.out.println (mismatch_string);
				mismatch_kmer_hash.put(mismatch_string.toString(), new Integer(1));
	    	}
	    }

	    return mismatch_kmer_hash;
	}

	// code adapted from (http://stackoverflow.com/questions/9591561/java-cartesian-product-of-a-list-of-lists)
	public static String[][] allUniqueCombinations (List<List<String>> lists) {
	    List<List<String>> combinations = product(lists);
	    int m = combinations.size();
	    int n = lists.size();
	    String[][] answer = new String[m][n];

	    for (int i = 0; i < m; i++){
	        for (int j = 0; j < n; j++){
	            answer[i][j] = combinations.get(i).get(j);
	        }
	    }
	    return answer;
	}

	public static List<List<String>> product (List<List<String>> lists) {
	    List<List<String>> result = new ArrayList<List<String>>();
	    result.add(new ArrayList<String>());

	    for (List<String> e : lists) {
	        List<List<String>> tmp1 = new ArrayList<List<String>>();
	        for (List<String> x : result) {
	            for (String y : e) {
	                List<String> tmp2 = new ArrayList<String>(x);
	                tmp2.add(y);
	                // System.out.println (tmp2);
	                tmp1.add(tmp2);
	            }
	        }
	        result = tmp1;
	    }
	    return result;
	}

	// creates a hash of kmer string sequence with the mapped value being the frequency of occurrences
	public static HashMap<String, Integer> build_kmer_hash (String genome, int kmer_length){
		HashMap<String, Integer> kmer_hash = new HashMap<String, Integer> ();
		for (int i = 0; i < genome.length() - kmer_length + 1; i++){
			String kmer_key = genome.substring (i, i + kmer_length);
			Integer count = kmer_hash.get(kmer_key);
			if (count != null) {
				kmer_hash.put (kmer_key, new Integer(count + 1));
			}else{
				kmer_hash.put (kmer_key, new Integer(1));
			}
		}
		return kmer_hash;
	}

	// code adapted from http://stackoverflow.com/questions/3584625/merge-hashmaps-retaining-values-java
	public static HashMap<String, Integer> reduceMaps (HashMap<String, Integer> map1, HashMap<String, Integer> map2){
		HashMap<String, Integer> merged = new HashMap<String, Integer>();

		for (String x : map1.keySet()) {
		   	Integer y = map2.get(x);
		   	if (y == null) {
		      	merged.put(x, map1.get(x));
		   	} else {
		      	merged.put(x, map1.get(x)+y);
		   	}
		}

		for (String x : map2.keySet()) {
		   	if (merged.get(x) == null) {
		      	merged.put(x, map2.get(x));
		   	}
		}

		return merged;
	}

	public static String reverse_complement (String dna_seq){
		String reverse_complement_string = "";

		for (int base : dna_seq.toCharArray()){
			switch (base){
				case 'A':	reverse_complement_string = "T" + reverse_complement_string;
							break;
				case 'T':	reverse_complement_string = "A" + reverse_complement_string;
							break;
				case 'C':	reverse_complement_string = "G" + reverse_complement_string;
							break;
				case 'G':	reverse_complement_string = "C" + reverse_complement_string;
							break;
			}
		}
		return reverse_complement_string;
	}

	public static int hamming_distance (String seq1, String seq2){

		if (seq1.length()!=seq2.length()){
			System.out.println("Sizes of sequence do not match");
			System.exit(0);
		}
		
		int substitution_distance = 0;
		for (int i=0; i<seq1.length(); i++){
			if (seq1.charAt(i)!=seq2.charAt(i)){
				substitution_distance++;
			}
		}
		
		return substitution_distance;
	}

	public static String count_dna (String dna_seq){
		int countA = 0;
		int countC = 0;
		int countG = 0;
		int countT = 0;
			
		for (int base : dna_seq.toCharArray()){
			switch(base){
				case 'A': 	countA++;
					 		break;
				case 'C':	countC++;
							break;
				case 'G':	countG++;
							break;
				case 'T':	countT++;
							break; 
			}
		}
		return Integer.toString(countA)+" "+Integer.toString(countC)+" "+Integer.toString(countG)+" "+Integer.toString(countT);
	}

	public static String dna_to_rna (String dna_seq){
		String rna_seq = "";

		for (int base : dna_seq.toCharArray()){
			if (((char)base)=='T'){
				rna_seq += "U";
			}else{
				rna_seq += (char)base;
			}
		}
		return rna_seq;
	}

	public static String rna_to_protein (String rna_seq){
		if ((rna_seq.length()-1) % 3 != 0){
			System.out.println("RNA sequence not in multiple of 3");
			System.exit(0);
		}
		
		int pos = 0;
		String codon = "";
		String amino_acid = "";
		String aa_seq = "";
		
		while(pos < rna_seq.length()){
			codon = rna_seq.substring(pos, pos + 3);
				
			switch(codon){
				case "UUU": 
				case "UUC":	amino_acid = "F";
								break;
				case "UUA":	
				case "UUG":	
				case "CUU":
				case "CUC":
				case "CUA":
				case "CUG":	amino_acid = "L";
								break;
				case "UCU":
				case "UCC":
				case "UCA":
				case "UCG":
				case "AGU":
				case "AGC":	amino_acid = "S";
								break;
				case "UAU":
				case "UAC":	amino_acid = "Y";
								break;
				case "UGU":
				case "UGC":	amino_acid = "C";
								break;
				case "UGG":	amino_acid = "W";
								break;
				case "CCU":
				case "CCC":
				case "CCA":
				case "CCG":	amino_acid = "P";
								break;
				case "CAU":
				case "CAC":	amino_acid = "H";
								break;
				case "CAA":
				case "CAG":	amino_acid = "Q";
								break;
				case "CGU":
				case "CGC":
				case "CGA":
				case "CGG":
				case "AGA":
				case "AGG":	amino_acid = "R";
								break;
				case "AUU":
				case "AUC":
				case "AUA":	amino_acid = "I";
								break;
				case "AUG":	amino_acid = "M";
								break;
				case "ACU":
				case "ACC":
				case "ACA":
				case "ACG":	amino_acid = "T";
								break;
				case "AAU":
				case "AAC": amino_acid = "N";
								break;
				case "AAA":
				case "AAG":	amino_acid = "K";
								break;
				case "GUU":
				case "GUC":
				case "GUA":
				case "GUG":	amino_acid = "V";
								break;
				case "GCU":
				case "GCC":
				case "GCA":
				case "GCG":	amino_acid = "A";
								break;
				case "GAU":
				case "GAC":	amino_acid = "D";
								break;
				case "GAA":
				case "GAG": amino_acid = "E";
								break;
				case "GGU":
				case "GGC":
				case "GGA":
				case "GGG":	amino_acid = "G";
								break;
				case "UAA":
				case "UAG":
				case "UGA":	pos = rna_seq.length();
								amino_acid = "";	
								break;
			}
			aa_seq = aa_seq + amino_acid;
			// System.out.print(amino_acid);
			pos = pos+3;
		}
		return aa_seq;
	}

	public static boolean check_for_duplicates(Integer[] list){
		Set<Integer> temp_set = new HashSet<Integer>();
		for (Integer i : list){
			if (temp_set.contains(i.intValue())){
				return true;
			} else {
				temp_set.add(i.intValue());
			}
		}
		return false;
	}
}

