#!/usr/bin/env python

import sys
import argparse
import collections   #defaultdict
import time
import re

from read_mapping_approximate_matching_lib import * #assuming same directory

if __name__ == "__main__":

	parser = argparse.ArgumentParser(description = 'Bioinformatics python practice')
	parser.add_argument('--input_path', metavar='path', type=str, help='Input file path')
	parser.add_argument('--scoring_matrix_path', metavar='path', type=str, help='Scoring matrix file path')
	parser.add_argument('--command', metavar='string', type=str, help='Enter what to do to input')
	args = parser.parse_args()

	textfile = open(args.input_path,'r')
	text = textfile.read()

	if (args.command == 'build_trie'):
		string_list = text.strip().split('\n')

		mytrie = construct_trie (string_list)
		mytrie.depth_first_traversal ()
		output_str = ''
	elif (args.command == 'exact_matching_trie'):
		string_list = text.strip().split('\n')

		text = string_list[0].strip()
		patterns = string_list[1:]

		mytrie = construct_trie (patterns)
		pattern_locations = exact_matching_trie (mytrie, text)

		output_str = ''
		for pattern in patterns:
			occurrences = pattern_locations[pattern]
			occurrences = map(str, occurrences)
			output_str += ' '.join(occurrences) + '\n'
	elif (args.command == 'longest_repeat_suffix_trie'):
		seq = text.strip()

		suffix_trie = Suffix_Trie (seq)
		longest_repeat = suffix_trie.longest_repeat ()
		output_str = longest_repeat
	elif (args.command == 'longest_repeat_suffix_tree'):
		seq = text.strip()

		suffix_tree = Suffix_Tree (seq)
		suffix_list = suffix_tree.enumerate_suffix (seq)   # list sorted by longest to shortest substring
		suffix_tree.build_suffix_tree_naive (suffix_list)   # populate suffix tree
		longest_repeat = suffix_tree.longest_repeat ()
		output_str = longest_repeat
	elif (args.command == 'suffix_tree_labels'):
		seq = text.strip()

		suffix_tree = Suffix_Tree (seq)
		suffix_list = suffix_tree.enumerate_suffix (seq)   # list sorted by longest to shortest substring

		# builds a suffix trie
		# for suffix in suffix_list:
		# 	suffix_tree.append (suffix[0], suffix[1])  # passes both the suffix string and the index where it starts
		
		# suffix_tree.trie_to_tree ()   # construct suffix tree from condensing suffix trie, really bad way

		suffix_tree.build_suffix_tree_naive (suffix_list)
		node_labels = suffix_tree.retrieve_node_labels ()
		output_str = ''
		for label in node_labels:
			if '$' in label:
				output_str += label[:label.find('$')+1] + '\n'
			else:
				output_str += label + '\n'
	elif (args.command == 'longest_common_substring'):
		[seq1, seq2] = text.strip().split('\n')

		suffix_tree = Suffix_Tree (seq1)   # really does not matter which sequence is passed in
		suffix_list_seq1 = suffix_tree.enumerate_suffix (seq1)
		suffix_list_seq2 = suffix_tree.enumerate_suffix (seq2)
		
		suffix_tree.build_suffix_tree_multiple_seq (suffix_list_seq1, 1)
		suffix_tree.build_suffix_tree_multiple_seq (suffix_list_seq2, 2)

		longest_common_subsequence = suffix_tree.longest_common_subsequence ([1,2])   # seq1 = 1, seq2 = 2
		output_str = longest_common_subsequence
	elif (args.command == 'build_suffix_array'):
		seq = text.strip()
		start_time = time.time ()

		suffix_array = Suffix_Array (seq)
		sorted_suffix_index = suffix_array.sorted_suffix_index
		# sorted_suffix_index = sorted(range(len(seq)), cmp= lambda i,j: cmp (seq[i:], seq[j:]))  # not using class object, gives about the same time
		# sorted_suffix_index = sorted(range(len(seq)), key= lambda i: seq[i:])   # less memory efficient than above
		output_str = str(sorted_suffix_index)
		end_time = time.time ()
		elapsed_time = end_time - start_time
		print 'Elapsed Time : ' + str(elapsed_time)
		# output_str = output_str.replace('[','').replace(']','')
		output_str = re.sub('[\[\]]', '', output_str)

	print output_str








