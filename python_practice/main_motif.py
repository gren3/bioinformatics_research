#!/usr/bin/env python

import sys
import argparse
import re
import string
import collections   #defaultdict
import time

from process_motif import * #assuming same directory

if __name__ == "__main__":

	parser = argparse.ArgumentParser(description = 'Bioinformatics python practice')
	parser.add_argument('--input_path', metavar='path', type=str, help='Input file path')
	parser.add_argument('--kmer_length', metavar='int', type=int, help='Kmer length of interest')
	parser.add_argument('--command', metavar='string', type=str, help='Enter what to do to input')
	parser.add_argument('--input_pattern', metavar='string', type=str, help='Enter a pattern to match within DNA')
	parser.add_argument('--hamm_bound', metavar='int', type=int, help='Hamming distance allowed from pattern')
	parser.add_argument('--iterations', metavar='int', type=int, help='Number of steps to go through')
	parser.add_argument('--repeats', metavar='int', type=int, help='Number of repeats to go through')
	args = parser.parse_args()

	textfile = open(args.input_path,'r')
	text = textfile.read()
	my_motif = Motif (text)

	if (args.command == 'enumerate_motif'):
		dna_list = my_motif.seq.strip().split('\n')
		start_time = time.time ()
		motif_list = my_motif.common_motif_naive (dna_list, args.kmer_length, args.hamm_bound)
		end_time = time.time ()
		elapsed_time = end_time - start_time
		print 'Elapsed Time : ' + str(elapsed_time)

		output_str = ''
		for motif in sorted(motif_list):
			output_str = output_str + motif + ' '
	elif (args.command == 'score_motif_hamming'):
		dna_list = my_motif.seq.strip().split('\n')
		start_time = time.time ()
		motif = my_motif.least_hamm_motif (dna_list, args.kmer_length)
		end_time = time.time ()
		elapsed_time = end_time - start_time
		print 'Elapsed Time : ' + str(elapsed_time)

		output_str = motif
	elif (args.command == 'score_motif_profile'):
		input_list = my_motif.seq.strip().split('\n')
		dna_seq = input_list[0].strip()
		profile_table = []
		# populating profile dictionary
		for i in xrange(1,len(input_list)):
			probability = input_list[i].strip().split(' ')
			profile = {'A':float(probability[0]), 'C':float(probability[1]), 'G':float(probability[2]), 'T':float(probability[3])}
			profile_table.append (profile)
		start_time = time.time ()
		motif = my_motif.motif_by_profile (profile_table, dna_seq, args.kmer_length)
		end_time = time.time ()
		elapsed_time = end_time - start_time
		print 'Elapsed Time : ' + str(elapsed_time)

		output_str = motif
	elif (args.command == 'greedy_motif_selection'):
		input_list = my_motif.seq.strip().split('\n')
		start_time = time.time ()
		best_motif_list = my_motif.motif_selection_greedy (input_list, args.kmer_length, False)
		end_time = time.time ()
		elapsed_time = end_time - start_time
		print 'Elapsed Time : ' + str(elapsed_time)

		output_str = ''
		for motif in best_motif_list:
			output_str = output_str + motif + '\n'
	elif (args.command == 'greedy_motif_selection_pseudocount_padding'):
		input_list = my_motif.seq.strip().split('\n')
		start_time = time.time ()
		best_motif_list = my_motif.motif_selection_greedy (input_list, args.kmer_length, True)
		end_time = time.time ()
		elapsed_time = end_time - start_time
		print 'Elapsed Time : ' + str(elapsed_time)

		output_str = ''
		for motif in best_motif_list:
			output_str = output_str + motif + '\n'
	elif (args.command == 'randomized_motif_selection'):
		input_list = my_motif.seq.strip().upper().split('\n')
		start_time = time.time ()
		best_motif_list = my_motif.motif_selection_randomized (input_list, args.kmer_length, args.iterations)
		end_time = time.time ()
		elapsed_time = end_time - start_time
		print 'Elapsed Time : ' + str(elapsed_time)

		output_str = ''
		for motif in best_motif_list:
			output_str = output_str + motif + '\n'
	elif (args.command == 'gibbs_motif_selection'):
		input_list = my_motif.seq.strip().upper().split('\n')
		start_time = time.time ()
		best_motif_list = my_motif.repeat_gibbs (args.repeats, input_list, args.kmer_length, args.iterations)
		end_time = time.time ()
		elapsed_time = end_time - start_time
		print 'Elapsed Time : ' + str(elapsed_time)

		output_str = ''
		for motif in best_motif_list:
			output_str = output_str + motif + '\n'
	print output_str



