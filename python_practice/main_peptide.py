#!/usr/bin/env python

import sys
import argparse
import re
import string
import collections   #defaultdict
import time

from process_peptide import * #assuming same directory

if __name__ == "__main__":

	parser = argparse.ArgumentParser(description = 'Bioinformatics python practice')
	parser.add_argument('--input_path', metavar='path', type=str, help='Input file path')
	parser.add_argument('--kmer_length', metavar='int', type=int, help='Kmer length of interest')
	parser.add_argument('--command', metavar='string', type=str, help='Enter what to do to input')
	parser.add_argument('--input_pattern', metavar='string', type=str, help='Enter a pattern to match within DNA')
	parser.add_argument('--hamm_bound', metavar='int', type=int, help='Hamming distance allowed from pattern')
	parser.add_argument('--cut_off', metavar='int', type=int, help='Cutoff point for number of cyclopeptide sequences to keep')
	parser.add_argument('--cut_off_conv', metavar='int', type=int, help='Cutoff point for number of convolution weights to use')
	args = parser.parse_args()

	textfile = open(args.input_path,'r')
	text = textfile.read()
	my_peptide = Peptide (text)

	if (args.command == 'rna_to_amino_acid'):
		output_str = my_peptide.rna_to_amino_acid ()
	elif (args.command == 'match_peptide'):
		output_list = my_peptide.match_peptide (args.input_pattern)
		output_str = ''
		for output in output_list:
			output_str = output_str + output + '\n'
	elif (args.command == 'peptide_mass_spec'):
		weight_list = my_peptide.generate_peptide_mass_spec (my_peptide.seq)
		output_str = ''
		for weight in weight_list:
			output_str = output_str + str(weight) + ' '
	elif (args.command == 'cyclopeptide_sequence'):
		spectrum_list = my_peptide.seq.split(' ')
		peptide_list = my_peptide.cyclopeptide_sequencing (spectrum_list)  # sequence is the spectrum of peptide
		output_str = ''
		for peptide in peptide_list:
			peptide_str = ''
			for i in xrange(0,len(peptide)-1):
				peptide_str = peptide_str + str(peptide[i]) + '-'
			peptide_str = peptide_str + str(peptide[-1])  # last weight within list
			output_str = output_str + peptide_str + ' '
	elif (args.command == 'ranked_cyclopeptide_sequence'):
		spectrum_list = my_peptide.seq.split(' ')
		spectrum_list = [int(x) for x in spectrum_list]
		start_time = time.time ()
		peptide_list = my_peptide.ranked_cyclopeptide_sequencing (spectrum_list, args.cut_off)
		end_time = time.time ()
		elapsed_time = end_time - start_time
		print 'Elapsed Time : ' + str(elapsed_time)
		
		output_str = ''
		for peptide in peptide_list:
			peptide_str = ''
			for i in xrange(0,len(peptide)-1):
				peptide_str = peptide_str + str(peptide[i]) + '-'
			peptide_str = peptide_str + str(peptide[-1])  # last weight within list
			output_str = output_str + peptide_str + ' '
	elif (args.command == 'spectral_convolute'):
		spectrum_list = my_peptide.seq.split(' ')
		spectrum_list = [int(x) for x in spectrum_list]
		output_str = ''
		convolution_results = my_peptide.spectral_convolution (spectrum_list)
		convolution_results = sorted (convolution_results)
		for num in convolution_results:
			output_str = output_str + str(num) + ' '
	elif (args.command == 'convoluted_ranked_cyclopeptide_sequence'):
		spectrum_list = my_peptide.seq.split(' ')
		spectrum_list = [int(x) for x in spectrum_list]
		spectrum_list = sorted (spectrum_list)
		peptide_list = my_peptide.ranked_convolution_cyclopeptide_sequencing (spectrum_list, args.cut_off_conv, args.cut_off)
		output_str = ''
		for peptide in peptide_list:
			peptide = [str(weight) for weight in peptide]
			peptide_str = '-'.join(peptide)
			output_str = output_str + peptide_str + ' '
	print output_str
