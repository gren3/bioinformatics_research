#!/usr/bin/env python

import sys
import argparse
import re
import string
import collections   #defaultdict
from collections import Counter
from operator import add
import itertools

class DNA(object):
    def __init__(self, bp_string):
        self.seq = bp_string.strip()

    # building dictionary of kmer occurrence counts
    def build_kmer_dict (self, seq, kmer_length):
    	kmer_dict = collections.defaultdict(int)
    	for i in xrange (len(seq) - kmer_length + 1):
    		kmer_dict[seq[i:i+kmer_length]] += 1
    	return kmer_dict

    # building dictionary of kmer occurrence locations
    def build_kmer_dict_indexed (self, kmer_length):
        kmer_dict = collections.defaultdict(list)
        for i in xrange (len(self.seq) - kmer_length + 1):
            kmer_dict[self.seq[i:i+kmer_length]].append(i)
        return kmer_dict

    def find_common_kmer (self):
    	max_count = 0
    	for key in self.kmer_dict:
    		if (self.kmer_dict[key] > max_count):
    			max_count = self.kmer_dict[key]
    	
    	max_count_kmers = []
    	for key in self.kmer_dict:
    		if (self.kmer_dict[key] == max_count):
    			max_count_kmers.append (key)
    	return max_count_kmers

    def reverse_complement (self,dna_seq):
        complement = string.maketrans('ACTG','TGAC')
        return string.translate(dna_seq, complement)[::-1]

    def pattern_match (self, pattern_str):
        match_ind = []
        text_length = len(self.seq)
        pattern_length = len(pattern_str)
        for i in xrange(text_length - pattern_length):
            if (self.seq[i:i+pattern_length] == pattern_str):
                match_ind.append (i)

        return match_ind

    # not very efficient
    def clump_find (self, clump_length, kmer_length, occurrence):
        total_length = len(self.seq)
        Lt_clump_seq = collections.defaultdict(int)

        for i in xrange(total_length-clump_length):
            kmer_dict = self.build_kmer_dict (kmer_length)
            for key in kmer_dict:
                if (kmer_dict[key] >= occurrence):
                    Lt_clump_seq[key] = 1  # using a dict to prevent duplicates

        return Lt_clump_seq

    # C -1, G +1
    def build_gc_skew_list (self, dna_seq):
        skew_list = [0]  # beginning with no skew
        skew_sum = 0
        dna_seq = dna_seq.lstrip()
        for i in xrange(len(dna_seq)):
            if (dna_seq[i] == 'C'):
                skew_sum -= 1
            elif (dna_seq[i] == 'G'):
                skew_sum += 1
            skew_list.append(skew_sum)
        return skew_list

    def hamming_distance (self, seq1, seq2):
        assert len(seq1) == len(seq2)
        hamm_dist = 0
        for i in xrange(len(seq1)):
            if (seq1[i] != seq2[i]):
                hamm_dist += 1

        return hamm_dist

    # building dictionary of all possible k-mer combos
    def build_kmer_dict_w_mismatch_old (self, dna_seq, kmer_length, hamm_dist):
        bp = ['A','C','G','T']
        dna = kmer_length*[bp]
        kmers = list(itertools.product(*dna))
        for i in xrange(len(kmers)):
            kmers[i] = ''.join(kmers[i])
        
        kmer_dict = {}
        for k in kmers:
            kmer_dict[k] = 0

        for j in xrange (len(dna_seq) - kmer_length + 1):
            for key in kmer_dict:
                if (self.hamming_distance(dna_seq[j:j+kmer_length],key) <= hamm_dist):
                    kmer_dict[key] += 1

        max_count = 0
        for key in kmer_dict:
            if (kmer_dict[key] > max_count):
                max_count = kmer_dict[key]

        max_count_kmers = []
        for key in kmer_dict:
            if (kmer_dict[key] == max_count):
                max_count_kmers.append (key)
        return max_count_kmers
        # return kmer_dict

    # generates all patterns within a certain hamming distance away from given sequence
    def build_kmer_dict_w_mismatch (self, dna_seq, hamm_dist):
        mismatch_combos = [''.join(combo) for combo in itertools.product('ACTG', repeat=hamm_dist)]
        # print mismatch_combos
        mismatch_locations_list = list(itertools.product(range(len(dna_seq)), repeat=hamm_dist))

        mismatch_locations_list_nodup = []
        # saving list items without duplicated tuple elements
        for l in mismatch_locations_list:   # l is a tuple
          duplicated = [x for x, y in collections.Counter(l).items() if y > 1]
          if (len(duplicated) == 0):
            mismatch_locations_list_nodup.append(l)
        # print mismatch_locations_list_nodup

        kmer_w_mismatch_dict = collections.defaultdict(int)
        for i in xrange(len(mismatch_locations_list_nodup)):  # sequence of list index numbers
          for j in xrange(len(mismatch_combos)):  # sequence of base pair combos
            kmer = list (dna_seq)
            mismatch_locations = mismatch_locations_list_nodup[i]
            for k in xrange(len(mismatch_locations)):  # tuple
              kmer[mismatch_locations[k]] = mismatch_combos[j][k]
            kmer = ''.join(kmer)
            kmer_w_mismatch_dict[kmer] = 1

        return kmer_w_mismatch_dict

    def build_kmer_dict_w_mismatch_genome (self, dna_genome, kmer_length, hamm_dist):
        kmer_dict = self.build_kmer_dict (dna_genome, kmer_length)  # values are the number of occurrences of kmer
        kmer_w_mismatch_dict = {}
        for kmer in kmer_dict:
            occurrences = kmer_dict[kmer]
            # goes through all hamming distances <= hamm_dist
            temp_mismatch_dict = {}
            for dist in xrange(hamm_dist+1):
                temp_mismatch_dict.update(self.build_kmer_dict_w_mismatch(kmer,dist))
            for key in temp_mismatch_dict:
                temp_mismatch_dict[key] = temp_mismatch_dict[key] * occurrences
            temp = [kmer_w_mismatch_dict, temp_mismatch_dict]
            kmer_w_mismatch_dict = reduce(add, (Counter(dict(x)) for x in temp))
        return kmer_w_mismatch_dict

    def find_common_kmer_w_mismatch (self, dna_genome, kmer_length, hamm_dist):
        kmer_dict = self.build_kmer_dict_w_mismatch_genome (dna_genome, kmer_length, hamm_dist)

        max_count = 0
        for key in kmer_dict:
            if (kmer_dict[key] > max_count):
                max_count = kmer_dict[key]

        max_count_kmers = []
        for key in kmer_dict:
            # print key + " : " + str(kmer_dict[key])
            if (kmer_dict[key] == max_count):
                max_count_kmers.append (key)
        return max_count_kmers

    # for checking where substitution mismatches occur in genome, return the list of indices
    def check_kmer_w_mismatch (self, dna_genome, kmer, hamm_dist):
        index_of_match = []
        for i in xrange(len(dna_genome) - len(kmer) + 1):
            if (self.hamming_distance(dna_genome[i:i+len(kmer)], kmer) <= hamm_dist):
                index_of_match.append (i)
        return index_of_match

    def find_common_kmer_w_mismatch_revcomp (self, dna_genome, kmer_length, hamm_dist):
        kmer_dict = self.build_kmer_dict_w_mismatch_genome (dna_genome, kmer_length, hamm_dist)
        revcomp_kmer_dict = {}
        
        for key, val in kmer_dict.items():
            revcomp_kmer_dict[self.reverse_complement(key)] = val        
        temp = [kmer_dict, revcomp_kmer_dict]
        kmer_w_mismatch_dict = reduce(add, (Counter(dict(x)) for x in temp))

        max_count = 0
        for key in kmer_w_mismatch_dict:
            if (kmer_w_mismatch_dict[key] > max_count):
                max_count = kmer_w_mismatch_dict[key]

        max_count_kmers = []
        for key in kmer_dict:
            # print key + " : " + str(kmer_dict[key])
            if (kmer_w_mismatch_dict[key] == max_count):
                max_count_kmers.append (key)
                max_count_kmers.append (self.reverse_complement(key))
        return max_count_kmers

    def rna_to_amino_acid (self, rna_seq):
        codon_table = {'UUU':'F', 'UUC':'F', 'UUA':'L', 'UUG':'L',
                    'UCU':'S', 'UCC':'S', 'UCA':'S', 'UCG':'S',
                    'UAU':'Y', 'UAC':'Y', 'UAA':'STOP', 'UAG':'STOP',
                    'UGU':'C', 'UGC':'C', 'UGA':'STOP', 'UGG':'W',
                    'CUU':'L', 'CUC':'L', 'CUA':'L', 'CUG':'L',
                    'CCU':'P', 'CCC':'P', 'CCA':'P', 'CCG':'P',
                    'CAU':'H', 'CAC':'H', 'CAA':'Q', 'CAG':'Q',
                    'CGU':'R', 'CGC':'R', 'CGA':'R', 'CGG':'R',
                    'AUU':'I', 'AUC':'I', 'AUA':'I', 'AUG':'M',
                    'ACU':'T', 'ACC':'T', 'ACA':'T', 'ACG':'T',
                    'AAU':'N', 'AAC':'N', 'AAA':'K', 'AAG':'K',
                    'AGU':'S', 'AGC':'S', 'AGA':'R', 'AGG':'R',
                    'GUU':'V', 'GUC':'V', 'GUA':'V', 'GUG':'V',
                    'GCU':'A', 'GCC':'A', 'GCA':'A', 'GCG':'A',
                    'GAU':'D', 'GAC':'D', 'GAA':'E', 'GAG':'E',
                    'GGU':'G', 'GGC':'G', 'GGA':'G', 'GGG':'G'}

        ind = 0
        amino_acid_seq = ''
        while (ind <= len(rna_seq) - 3):
            if (codon_table[rna_seq[ind:ind+3]] != 'STOP'):
                amino_acid_seq = amino_acid_seq + codon_table[rna_seq[ind:ind+3]]
                ind = ind + 3
            else:
                break
        return amino_acid_seq

    def match_peptide (self, peptide_string):
        kmer_length = len(peptide_string)*3
        kmer_dict = self.build_kmer_dict (self.seq, kmer_length)  # build kmer dictionary of given dna sequence
        matched_seq = []
        for key in kmer_dict:
            # print key + ' ' + self.rna_to_amino_acid(self.dna_to_rna(key))
            if (self.rna_to_amino_acid(self.dna_to_rna(key)) == peptide_string):
                matched_seq.append (key)
            elif (self.rna_to_amino_acid(self.dna_to_rna(self.reverse_complement(key))) == peptide_string):
                matched_seq.append (key)
        return matched_seq

    def dna_to_rna (self, dna_string):
        return dna_string.upper().replace('T','U')

    def combination_all_sizes (self, seq):
        combos = []
        for i in xrange(len(seq)+1):
            combos.extend(list(itertools.combinations(seq, i)))
        return combos

    def peptide_weight (self, peptide):
        weight_table = {'G':57,'A':71,'S':87,'P':97,'V':99,
            'T':101,'C':103,'I':113,'L':113,
            'N':114,'D':115,'K':128,'Q':128,
            'E':129,'M':131,'H':137,'F':147,
            'R':156,'Y':163,'W':186}

        weight = 0
        for aa in peptide:  # assuming a peptide is represented by a tuple of amino acid letters
            weight += weight_table[aa]
        return weight

    def peptide_combos_weight (self, initial_peptide):
        peptide_combos = self.combination_all_sizes (initial_peptide)

        weight_list = []
        for peptide in peptide_combos:  # peptide combinations of length 0 to len(initial_peptide)
            weight_list.append (self.peptide_weight (peptide))
        return sorted(weight_list)

