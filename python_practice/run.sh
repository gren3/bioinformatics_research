

# python main.py --input_path 'input_kmers.txt' --kmer_length 11 --command 'most_freq_kmer' >output_kmers.txt
# python main.py --input_path 'input_rev_comp.txt' --kmer_length 1 --command 'rev_comp' >output_rev_comp.txt
# python main.py --input_path 'input_match_pattern.txt' --kmer_length 1 --command 'pattern_match' --input_pattern 'CTTGATCAT' >output_match_pattern.txt
# python main.py --input_path 'input_clump_find.txt' --kmer_length 12 --command 'clump_find' --clump_length 579 --kmer_occ 18 >output_clump_find.txt
# python main.py --input_path 'input_gc_skew.txt' --kmer_length 1 --command 'gc_skew' >output_gc_skew.txt
# python main.py --input_path 'input_hamming_distance.txt' --input_pattern 'CATTGGG' --hamm_bound 5 --kmer_length 7 --command 'mismatch_hamming' >output_hamming_distance.txt
# python main.py --input_path 'input_most_freq_kmer_w_mismatch.txt' --hamm_bound 2 --kmer_length 10 --command 'most_freq_kmer_w_mismatch' >output_most_freq_kmer_w_mismatch.txt
# python main.py --input_path 'input_rna_to_amino_acid.txt' --kmer_length 10 --command 'rna_to_amino_acid' >output_rna_to_amino_acid.txt
# python main.py --input_path 'input_match_peptide.txt' --kmer_length 10 --input_pattern 'NWARVFLW' --command 'match_peptide' >output_match_peptide.txt

python main.py --input_path 'input_peptide_weight.txt' --kmer_length 10 --command 'peptide_weight' >output_peptide_weight.txt
