#!/usr/bin/env python

import sys
import argparse
import collections   #defaultdict
from collections import Counter
from operator import add
import itertools

class Genome(object):
    def __init__(self, genome_str):
        self.seq = genome_str.strip()

    def generate_reads_exact (self, dna_seq, read_length):
    	read_dict = {}
    	for i in xrange (len(dna_seq) - read_length + 1):
    		read_dict[dna_seq[i:i+read_length]] = 1
    	return sorted (read_dict.keys())

    def build_overlap_graph (self, kmer_list):
        prefix_dict = collections.defaultdict(list)   # maps a k - 1 string to a list of kmers with string as prefix

        for kmer in kmer_list:
            prefix = kmer[:-1]
            prefix_dict[prefix].append (kmer)

        graph_output = {}
        for kmer in kmer_list:
            suffix = kmer[1:]
            if suffix in prefix_dict:
                adjacent_kmers = prefix_dict[suffix]
                graph_output[kmer] = adjacent_kmers

        return graph_output   # should return a dictionary with key as kmer and value as the list of adjacent kmers

    def build_debruijn_graph_from_kmers (self, kmer_list):
        adjacency_mapping = collections.defaultdict(list)
        for kmer in kmer_list:
            prefix = kmer[:-1]
            suffix = kmer[1:]
            adjacency_mapping[prefix].append(suffix)

        return adjacency_mapping

    # same as previous method but without edges with identical nodes, ie. self-referencing
    def build_debruijn_graph_from_kmers_2 (self, kmer_list):
        adjacency_mapping = collections.defaultdict(list)
        for kmer in kmer_list:
            prefix = kmer[:-1]
            suffix = kmer[1:]
            if (prefix != suffix):
                adjacency_mapping[prefix].append(suffix)

        return adjacency_mapping

    def build_debruijn_graph (self, dna_seq, kmer_length):
        kmer_list = []
        for i in xrange (len(dna_seq) - kmer_length + 1):
            kmer_list.append (dna_seq[i:i+kmer_length])

        # adjacency_mapping = collections.defaultdict(list)
        # for kmer in kmer_list:
        #     prefix = kmer[:-1]
        #     suffix = kmer[1:]
        #     adjacency_mapping[prefix].append(suffix)

        # return adjacency_mapping
        return self.build_debruijn_graph_from_kmers (kmer_list)

    # graph is a list of tuples with in-coming and out-going edges.
    def find_eulerian_cycle(self, graph):
        cycle=[]
        graph = sorted(graph)
        cycle.append (graph[0][0])
        [cycle, found] = self.trace_connection (graph[0][0],graph,cycle)
        return cycle

    def trace_connection (self, cur_node, graph, cycle):
        found = False

        for edge in graph:
            if edge[0] == cur_node:
                # print 'current edge: ' + str(edge) + ' graph size: ' + str(len(graph))
                # print 'current cycle: ' + str(cycle)
                # inefficient having to keep many copies of cycle, valid or invalid
                temp_cycle = cycle[:]
                temp_cycle.append(edge[1])
                temp_graph = graph[:]
                temp_graph.remove(edge)
                [final_cycle, found] = self.trace_connection (edge[1], temp_graph, temp_cycle)
        if (len(graph) == 0):
            # print cycle
            return [cycle, True]
        elif (found):
            return [final_cycle, True]
        else:
            return [[], False]

    # construct dictionary of nodes
    def parse_graph (self, graph):
        graph_dict = collections.defaultdict(list)

        for edge in graph:
            graph_dict[edge[0]].append (edge[1])

        return graph_dict

    # graph is a list of tuples with in-coming and out-going edges. faster implementation for eulerian cycle solver
    def find_eulerian_cycle_2 (self, graph):
        graph_dict = self.parse_graph(graph)
        stack = []  # list used as a stack data structure, holds dictionaries
        cycle = []

        stack.append ({graph[0][0]: graph_dict[graph[0][0]]})   # append == push
        while len(stack) > 0:
            top = stack[-1]   # same as peek at the top of stack
            destination_nodes_list = top.values()[0]   # values() returns a list of lists, only need the first 1
            if (len(destination_nodes_list) > 0):
                destination_node = destination_nodes_list.pop()   # picks last destination, save it and remove it
                stack.append({destination_node: graph_dict[destination_node]})   # append == push
            else:
                # print stack.pop().keys()[0]   # reversed sequence for cycle
                cycle.insert(0, stack.pop().keys()[0])
        return cycle

    # pre-process graph to locate starting node for eulerian path traversal
    def find_path_start (self, graph):
        incoming_edges_dict = collections.defaultdict(int)
        outgoing_edges_dict = collections.defaultdict(int)

        for edge in graph:
            incoming_edges_dict[edge[1]] += 1
            outgoing_edges_dict[edge[0]] += 1

        for node in outgoing_edges_dict:
            num_outgoing = outgoing_edges_dict[node]
            if (num_outgoing - incoming_edges_dict[node] == 1):   # start node has 1 more outgoing edge than incoming
                # print 'Start node is : ' + node
                return node

    # graph is a list of tuples with in-coming and out-going edges.
    def find_eulerian_path (self, graph):
        graph_dict = self.parse_graph(graph)
        start_node = self.find_path_start(graph)   # start node has 1 more outgoing edge than incoming
        stack = []  # list used as a stack data structure, holds dictionaries
        path = []

        stack.append ({start_node: graph_dict[start_node]})   # append == push
        while len(stack) > 0:
            top = stack[-1]   # same as peek at the top of stack
            destination_nodes_list = top.values()[0]   # values() returns a list of lists, only need the first 1
            if (len(destination_nodes_list) > 0):
                destination_node = destination_nodes_list.pop()   # picks last destination, save it and remove it
                stack.append({destination_node: graph_dict[destination_node]})   # append == push
            else:
                # print stack.pop().keys()[0]   # reversed sequence for path
                path.insert(0, stack.pop().keys()[0])
        return path

    # graph is a list of tuples with in-coming and out-going edges. same as previous method...
    def reconstruct_string_by_eulerian_path (self, graph):
        graph_dict = self.parse_graph(graph)
        start_node = self.find_path_start(graph)   # start node has 1 more outgoing edge than incoming
        stack = []  # list used as a stack data structure, holds dictionaries
        path = []

        stack.append ({start_node: graph_dict[start_node]})   # append == push
        while len(stack) > 0:
            top = stack[-1]   # same as peek at the top of stack
            destination_nodes_list = top.values()[0]   # values() returns a list of lists, only need the first 1
            if (len(destination_nodes_list) > 0):
                destination_node = destination_nodes_list.pop()   # picks last destination, save it and remove it
                stack.append({destination_node: graph_dict[destination_node]})   # append == push
            else:
                # print stack.pop().keys()[0]   # reversed sequence for path
                path.insert(0, stack.pop().keys()[0])
        return path

    def generate_binary_kmers (self, kmer_length):
        kmer_combos = [''.join(combo) for combo in itertools.product('01', repeat=kmer_length)]
        return kmer_combos

    # returns a list of graph connections, as in edges between nodes
    def kmers_to_graph (self, kmer_list):
        adjacency_map = self.build_debruijn_graph_from_kmers (kmer_list)
        graph_output = []

        for key in sorted(adjacency_map.iterkeys()):
            adjacent_strings = sorted (list(set(adjacency_map[key])))
            for destination_node in adjacent_strings:
                graph_output.append([key, destination_node])
        return graph_output

    # graph is a list of tuples with in-coming and out-going edges.
    def construct_binary_circular_string (self, graph):
        cycle_list = self.find_eulerian_cycle_2 (graph)
        return cycle_list


    def reconstruct_string_by_paired_reads (self, kmer_length, gap_length, paired_graph):
        path_list = self.find_eulerian_path (paired_graph)

        # initializing
        [forward_seq, backward_seq] = path_list[0].split('->')

        # print 'forward: ' + forward_seq + ' backward: ' + backward_seq
        for i in xrange(1, len(path_list)):
            [forward_string, backward_string] = path_list[i].split('->')
            # print 'forward: ' + forward_string + ' backward: ' + backward_string
            forward_seq += forward_string[-1]
            backward_seq += backward_string[-1]

        return forward_seq[:kmer_length + gap_length] + backward_seq

    # returns a list of contigs within debruijn graph
    def find_contigs (self, edges):
        adjacency_map = self.build_debruijn_graph_from_kmers (edges)

        # keep record of indegrees/outdegrees of nodes
        incoming_nodes_dict = collections.defaultdict(int)
        outgoing_nodes_dict = collections.defaultdict(int)
        visited = {}

        for edge in edges:
            outgoing_nodes_dict[edge[:-1]] += 1
            incoming_nodes_dict[edge[1:]] += 1
            # keep tab of which node has been visited, 0 == not visited, 1 == visited
            visited[edge[:-1]] = 0
            visited[edge[1:]] = 0

        stack = []
        contig_paths = []

        for node in visited:   # visited dictionary contains all nodes in graph
            # print 'node: ' + node
            if ((outgoing_nodes_dict[node] != incoming_nodes_dict[node] or (outgoing_nodes_dict[node] != 1 or incoming_nodes_dict[node] != 1) ) and visited[node] == 0):
                for outgoing_node in adjacency_map[node]:
                    stack.append ([node, outgoing_node])   # pushing the pair [start_node, destination_node] onto stack
                    visited[node] = 1
            while (len(stack) > 0):
                contig_path = stack.pop()
                cur_node = contig_path[-1]
                while (incoming_nodes_dict[cur_node] == 1 and outgoing_nodes_dict[cur_node] == 1 and visited[cur_node] == 0):
                    visited[cur_node] == 1
                    cur_node = adjacency_map[cur_node][0]   # should just have one anyways
                    contig_path.append (cur_node)
                # print str(contig_path) + ' node : ' + node
                contig_paths.append (contig_path)
            # print visited
        return contig_paths

