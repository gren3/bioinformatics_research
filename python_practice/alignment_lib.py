#!/usr/bin/env python

import sys
import argparse
import collections   #defaultdict
from collections import Counter
from operator import add
import itertools
import numpy

# a dynamic programming approach to finding the minimum number of coins for a change amount
def min_coin_change (amount, denomination_list):
    min_coins_for_amount_table = {}
    min_coins_for_amount_table[0] = 0

    amount_count = 1
    while amount_count <= amount:
        min_coins_for_amount_table[amount_count] = 1000000  # some insanely large number used here, symbolize infinity
        for i in xrange (len (denomination_list)):
            if amount_count >= denomination_list[i]:
                if min_coins_for_amount_table[amount_count - denomination_list[i]] + 1 < min_coins_for_amount_table[amount_count]:
                    min_coins_for_amount_table[amount_count] = min_coins_for_amount_table[amount_count - denomination_list[i]] + 1
        amount_count += 1

    return min_coins_for_amount_table[amount]

# dynamic programming for longest path/max flow problem, with given path weights
def manhattan_tourist (row, column, down_mat, right_mat):
    dp_matrix = numpy.ndarray((row+1,column+1))  # create an extra row/column for initial padding
    dp_matrix [0,0] = 0
    # print dp_matrix.shape
    # print down_mat.shape
    # print right_mat.shape

    # load in values down the first column
    for i in xrange (1, row+1):    # 1 to row inclusive
        dp_matrix [i, 0] = dp_matrix [i-1, 0] + down_mat [i-1, 0]
    # load in values down the first row
    for j in xrange (1, column+1):   # 1 to column inclusive
        dp_matrix [0, j] = dp_matrix [0, j-1] + right_mat [0, j-1]

    # print dp_matrix
    # solve for diagonal elements
    for i in xrange (1, row + 1):
        for j in xrange (1, column + 1):
            dp_matrix [i, j] = max (dp_matrix [i-1, j] + down_mat[i-1, j], dp_matrix [i, j-1] + right_mat[i, j-1])
    # print dp_matrix

    return dp_matrix [row, column]   # right bottom most number should be with the highest value


def longest_common_subsequence (seq1, seq2):
    lcs_mat = numpy.ndarray ((len(seq1)+1, len(seq2)+1))
    backtrack_path = numpy.ndarray ((len(seq1)+1, len(seq2)+1))

    for i in xrange (len(seq1)+1):
        lcs_mat [i, 0] = 0
        backtrack_path [i, 0] = -1
    for j in xrange (len(seq2)+1):
        lcs_mat [0, j] = 0
        backtrack_path [0, j] = -1
    for i in xrange (1, len(seq1)+1):
        for j in xrange (1, len(seq2)+1):
            if (seq1[i-1] == seq2[j-1]):
                diag_value = lcs_mat[i-1, j-1] + 1
            else:
                diag_value = lcs_mat[i-1, j-1]
            lcs_mat [i, j] = max (lcs_mat[i-1, j], lcs_mat[i, j-1], diag_value)
            if (lcs_mat [i, j] == lcs_mat[i-1, j]):
                backtrack_path [i, j] = 0   #'down'
            elif (lcs_mat [i, j] == lcs_mat[i, j-1]):
                backtrack_path [i, j] = 1    #'right'
            else:
                backtrack_path [i, j] = 2   #'diagonal'
    # print lcs_mat
    # print backtrack_path
    max_value = lcs_mat[len(seq1), len(seq2)]
    return [max_value, backtrack_path]

def dp_backtrack (backtrack_path, seq1, row_init, column_init):
    row = row_init
    column = column_init
    subseq = ''

    while (row != 0 and column != 0):
        # print subseq
        # print backtrack_path[row,column] 
        if (backtrack_path[row,column] == 0):    # down direction
            row -= 1     # go back by moving up a row
        elif (backtrack_path[row, column] == 1):   # right direction
            column -= 1    # go back by moving left a column
        else:
            subseq = seq1[row-1] + subseq
            row -= 1
            column -= 1    # go back diagonally

    return subseq

# assuming edges is a list of edge tuples i.e. [incoming_node, outgoing_node]
def topological_ordering (edges):
    source_dict = collections.defaultdict (list)
    sink_dict = collections.defaultdict (list)

    for edge in edges:
        source_dict [edge[1]].append(edge[0])
        sink_dict [edge[0]].append(edge[1])

    ordered_list = []
    candidates = []

    for node in sink_dict:
        if (len(source_dict[node]) == 0):
            candidates.append (node)
            source_dict.pop(node, None)

    while (len(candidates) > 0):
        cur_node = candidates.pop()
        ordered_list.append (cur_node)
        cur_node_sink_list = sink_dict[cur_node][:]   # creating a copy here, can't reference
        for sink_node in cur_node_sink_list:
            # print cur_node_sink_list
            if (len(source_dict[sink_node]) == 1):
                candidates.append (sink_node)
                source_dict.pop(sink_node, None)   # remove edge from source dictionary
            else:
                source_dict[sink_node].remove (cur_node)
            sink_dict[cur_node].remove (sink_node)
            # print 'source dict: ' + str(source_dict)
            # print 'sink dict: ' + str(sink_dict)

    if (len (source_dict) > 0):
        print 'graph is not a DAG'
        return []
    else:
        return ordered_list

# pseudo-code for topological ordering
# TOPOLOGICALORDERING(Graph)
#     List = empty list
#     Candidates = set of all nodes in Graph with no incoming edges
#     while Candidates is non-empty
#         select an arbitrary node b from Candidates
#         add b to the end of List and remove it from Candidates
#         for each outgoing edge from b to another node a
#             remove edge (b, a) from Graph
#             if a has no other incoming edges 
#                 add a to Candidates
#     if Graph has edges that have not been removed
#         return "the input graph is not a DAG"
#     else return List

# assuming source and sink are integers
def longest_weighted_path (edges, source, sink, edge_weights):
    source_dict = collections.defaultdict (list)
    sink_dict = collections.defaultdict (list)

    s_dict = {}
    for edge in edges:
        source_dict [edge[1]].append(edge[0])   # duplicated in topo ordering
        s_dict [int(edge[0])] = -100000
        s_dict [int(edge[1])] = -100000
    s_dict[source] = 0
    ordered_list = topological_ordering (edges)
    # print ordered_list

    start_ind = ordered_list.index (str(source))
    end_ind = ordered_list.index (str(sink))

    backtrack_graph = {}
    for i in xrange (start_ind+1, end_ind+1):
        max_weight = -100000
        cur_sink_node = ordered_list[i]
        max_weight_source = -1
        cur_source_node_list = source_dict[cur_sink_node]
        for source_node in cur_source_node_list:
            cur_weight = s_dict[int(source_node)] + int(edge_weights[(source_node, cur_sink_node)])
            if (cur_weight > max_weight):
                max_weight = cur_weight
                max_weight_source = source_node
        s_dict[int(cur_sink_node)] = max_weight
        # print s_dict
        backtrack_graph[cur_sink_node] = max_weight_source

    # print s_dict
    # print backtrack_graph

    # backtracking
    cur_node = str(sink)
    backtrack_path = []
    while int(cur_node) != source:
        predecessor_node = backtrack_graph[cur_node]
        backtrack_path.insert(0, cur_node)
        cur_node = predecessor_node
    backtrack_path.insert(0, str(source))
    return [s_dict[sink], backtrack_path]

# pseudo-code for longest path
# LONGESTPATH(Graph, source, sink)
#     for each node a in Graph
#         sa = -infinity
#     ssource = 0
#     topologically order Graph
#     for each node a from source to sink (following the topological order)
#         sa = max of all predecessors b of node a {sb + weight of edge from b to a}
#     return ssink

# a generalized version of longest common subsequence?
def global_alignment (seq1, seq2, indel_penalty, match_mismatch_scoring_dict):
    dp_mat = numpy.ndarray ((len(seq1)+1, len(seq2)+1))
    backtrack_path = numpy.ndarray ((len(seq1)+1, len(seq2)+1))
    dp_mat[0,0] = 0

    for i in xrange (1, len(seq1)+1):
        dp_mat [i, 0] = dp_mat[i-1, 0] - indel_penalty
        backtrack_path [i, 0] = 0
    for j in xrange (1, len(seq2)+1):
        dp_mat [0, j] = dp_mat[0, j-1] - indel_penalty
        backtrack_path [0, j] = 1
    for i in xrange (1, len(seq1)+1):
        for j in xrange (1, len(seq2)+1):
            if (seq1[i-1] == seq2[j-1]):
                diag_value = dp_mat[i-1, j-1] + float(match_mismatch_scoring_dict[seq1[i-1]][seq1[i-1]])
            else:
                diag_value = dp_mat[i-1, j-1] + float(match_mismatch_scoring_dict[seq1[i-1]][seq2[j-1]])
            dp_mat [i, j] = max (dp_mat[i-1, j] - indel_penalty, dp_mat[i, j-1] - indel_penalty, diag_value)
            if (dp_mat [i, j] == dp_mat[i-1, j]- indel_penalty):
                backtrack_path [i, j] = 0   #'down'
            elif (dp_mat [i, j] == dp_mat[i, j-1] - indel_penalty):
                backtrack_path [i, j] = 1    #'right'
            else:
                backtrack_path [i, j] = 2   #'diagonal'
    # print dp_mat
    # print backtrack_path
    max_value = dp_mat[len(seq1), len(seq2)]
    return [max_value, backtrack_path]

def global_align_backtrack (backtrack_path, seq1, seq2, row_init, column_init):
    row = row_init
    column = column_init
    output_seq1 = ''
    output_seq2 = ''

    while (row != 0 or column != 0):
        # print backtrack_path[row,column]   # prints out backtracking decision sequence
        if (backtrack_path[row,column] == 0):    # down direction, insertion in seq1
            output_seq2 = '-' + output_seq2
            output_seq1 = seq1[row-1] + output_seq1
            row -= 1     # go back by moving up a row
        elif (backtrack_path[row, column] == 1):   # right direction, insertion in seq2
            output_seq1 = '-' + output_seq1
            output_seq2 = seq2[column-1] + output_seq2
            column -= 1
        else:
            output_seq1 = seq1[row-1] + output_seq1
            output_seq2 = seq2[column-1] + output_seq2
            row -= 1
            column -= 1    # go back diagonally

    return [output_seq1, output_seq2]

# similar to global alignment, except for the initial row/columns and final element
def local_alignment (seq1, seq2, indel_penalty, match_mismatch_scoring_dict):
    dp_mat = numpy.ndarray ((len(seq1)+1, len(seq2)+1))
    backtrack_path = numpy.ndarray ((len(seq1)+1, len(seq2)+1))
    dp_mat[0,0] = 0
    max_mat_val = 0

    for i in xrange (1, len(seq1)+1):
        dp_mat [i, 0] = 0   # since it's local alignment, no penalty for indels
        backtrack_path [i, 0] = 0
    for j in xrange (1, len(seq2)+1):
        dp_mat [0, j] = 0  # since it's local alignment, no penalty for indels
        backtrack_path [0, j] = 1
    for i in xrange (1, len(seq1)+1):
        for j in xrange (1, len(seq2)+1):
            if (seq1[i-1] == seq2[j-1]):
                diag_value = dp_mat[i-1, j-1] + float(match_mismatch_scoring_dict[seq1[i-1]][seq1[i-1]])
            else:
                diag_value = dp_mat[i-1, j-1] + float(match_mismatch_scoring_dict[seq1[i-1]][seq2[j-1]])
            dp_mat [i,j] = max (0, dp_mat[i-1, j] - indel_penalty, dp_mat[i, j-1] - indel_penalty, diag_value)
            if (dp_mat[i,j] > max_mat_val):
                max_mat_val = dp_mat[i,j]
            if (dp_mat [i, j] == dp_mat[i-1, j] - indel_penalty):
                backtrack_path [i, j] = 0   #'down'
            elif (dp_mat [i, j] == dp_mat[i, j-1] - indel_penalty):
                backtrack_path [i, j] = 1    #'right'
            else:
                backtrack_path [i, j] = 2   #'diagonal'

    # print dp_mat
    # print backtrack_path
    return [max_mat_val, backtrack_path, dp_mat]

# backtracks until reaching 0,0 position or reaching a 0 score, starting from max point
def local_align_backtrack (backtrack_path, dp_mat, seq1, seq2):
    output_seq1 = ''
    output_seq2 = ''

    # locating initial row/column at where backtracking starts
    [max_val_row, max_val_col] = numpy.unravel_index(dp_mat.argmax(), dp_mat.shape)

    row = max_val_row
    column = max_val_col
    while (row != 0 or column != 0):
        # print backtrack_path[row,column]   # prints out backtracking decision sequence
        if (backtrack_path[row,column] == 0):    # down direction, insertion in seq1
            output_seq2 = '-' + output_seq2
            output_seq1 = seq1[row-1] + output_seq1
            row -= 1     # go back by moving up a row
        elif (backtrack_path[row, column] == 1):   # right direction, insertion in seq2
            output_seq1 = '-' + output_seq1
            output_seq2 = seq2[column-1] + output_seq2
            column -= 1
        else:
            output_seq1 = seq1[row-1] + output_seq1
            output_seq2 = seq2[column-1] + output_seq2
            row -= 1
            column -= 1    # go back diagonally
        if (dp_mat[row][column] == 0):
            break

    return [output_seq1, output_seq2]

# a slightly altered version of global alignment, 0 for match, 1 for mismatch and indels, take minimum
def edit_distance (seq1, seq2):
    dp_mat = numpy.ndarray ((len(seq1)+1, len(seq2)+1))
    backtrack_path = numpy.ndarray ((len(seq1)+1, len(seq2)+1))
    dp_mat[0,0] = 0

    for i in xrange (1, len(seq1)+1):
        dp_mat [i, 0] = dp_mat[i-1, 0] + 1  # indel penalty of +1
        backtrack_path [i, 0] = 0
    for j in xrange (1, len(seq2)+1):
        dp_mat [0, j] = dp_mat[0, j-1] + 1   # indel penalty of +1
        backtrack_path [0, j] = 1
    for i in xrange (1, len(seq1)+1):
        for j in xrange (1, len(seq2)+1):
            if (seq1[i-1] == seq2[j-1]):
                diag_value = dp_mat[i-1, j-1]    # match score of 0
            else:
                diag_value = dp_mat[i-1, j-1] + 1   # mismatch score of 1
            dp_mat [i, j] = min (dp_mat[i-1, j] + 1, dp_mat[i, j-1] + 1, diag_value)
            if (dp_mat [i, j] == dp_mat[i-1, j] + 1):
                backtrack_path [i, j] = 0   #'down'
            elif (dp_mat [i, j] == dp_mat[i, j-1] + 1):
                backtrack_path [i, j] = 1    #'right'
            else:
                backtrack_path [i, j] = 2   #'diagonal'
    min_value = dp_mat[len(seq1), len(seq2)]
    return [min_value, backtrack_path]

# allows the user to customize scoring/penalty values
def generic_global_alignment (seq1, seq2, match_score, indel_penalty, mismatch_penalty):
    dp_mat = numpy.ndarray ((len(seq1)+1, len(seq2)+1))
    backtrack_path = numpy.ndarray ((len(seq1)+1, len(seq2)+1))
    dp_mat[0,0] = 0

    for i in xrange (1, len(seq1)+1):
        dp_mat [i, 0] = dp_mat[i-1, 0] - indel_penalty
        backtrack_path [i, 0] = 0
    for j in xrange (1, len(seq2)+1):
        dp_mat [0, j] = dp_mat[0, j-1] - indel_penalty
        backtrack_path [0, j] = 1
    for i in xrange (1, len(seq1)+1):
        for j in xrange (1, len(seq2)+1):
            if (seq1[i-1] == seq2[j-1]):
                diag_value = dp_mat[i-1, j-1] + match_score
            else:
                diag_value = dp_mat[i-1, j-1] + mismatch_penalty
            dp_mat [i, j] = max (dp_mat[i-1, j] - indel_penalty, dp_mat[i, j-1] - indel_penalty, diag_value)
            if (dp_mat [i, j] == dp_mat[i-1, j]- indel_penalty):
                backtrack_path [i, j] = 0   #'down'
            elif (dp_mat [i, j] == dp_mat[i, j-1] - indel_penalty):
                backtrack_path [i, j] = 1    #'right'
            else:
                backtrack_path [i, j] = 2   #'diagonal'
    max_value = dp_mat[len(seq1), len(seq2)]
    return [max_value, backtrack_path]

# a slightly altered version of local alignment, +1 for match, -1 for mismatch and indels, take maximum
# assuming the second sequence is being fit to the first (seq2 is shorter)
def fitting_alignment (seq1, seq2):
    dp_mat = numpy.ndarray ((len(seq1)+1, len(seq2)+1))
    backtrack_path = numpy.ndarray ((len(seq1)+1, len(seq2)+1))
    dp_mat[0,0] = 0

    for i in xrange (1, len(seq1)+1):
        dp_mat [i, 0] = 0   # no penalty for indels at the starting edge of seq1
        backtrack_path [i, 0] = 0
    for j in xrange (1, len(seq2)+1):
        dp_mat [0, j] = dp_mat[0, j-1] - 1  # - 1 penalty for indels at starting edge of seq2
        backtrack_path [0, j] = 1
    for i in xrange (1, len(seq1)+1):
        for j in xrange (1, len(seq2)+1):
            if (seq1[i-1] == seq2[j-1]):
                diag_value = dp_mat[i-1, j-1] + 1  # + 1 for matching
            else:
                diag_value = dp_mat[i-1, j-1] - 1   # - 1 for mismatching
            dp_mat [i,j] = max (dp_mat[i-1, j] - 1, dp_mat[i, j-1] - 1, diag_value)
            if (dp_mat [i, j] == dp_mat[i-1, j] - 1):   # - 1 for indel
                backtrack_path [i, j] = 0   #'down'
            elif (dp_mat [i, j] == dp_mat[i, j-1] - 1):   # - 1 for indel
                backtrack_path [i, j] = 1    #'right'
            else:
                backtrack_path [i, j] = 2   #'diagonal'

    # print dp_mat
    return [backtrack_path, dp_mat]

# backtracks from the max value in the last column of dynamic programming matrix; if multiple, takes max value from the minimum row
def fitting_align_backtrack (backtrack_path, dp_mat, seq1, seq2):
    output_seq1 = ''
    output_seq2 = ''

    row = numpy.argmax(dp_mat[:,-1])   # starting row is the one with max value in the last column
    column = dp_mat.shape[1]-1   # starting column is the last column
    max_fitting_value = dp_mat[row][column]
    while (column != 0):
        # print backtrack_path[row,column]   # prints out backtracking decision sequence
        if (backtrack_path[row,column] == 0):    # down direction, insertion in seq1
            output_seq2 = '-' + output_seq2
            output_seq1 = seq1[row-1] + output_seq1
            row -= 1     # go back by moving up a row
        elif (backtrack_path[row, column] == 1):   # right direction, insertion in seq2
            output_seq1 = '-' + output_seq1
            output_seq2 = seq2[column-1] + output_seq2
            column -= 1
        else:
            output_seq1 = seq1[row-1] + output_seq1
            output_seq2 = seq2[column-1] + output_seq2
            row -= 1
            column -= 1    # go back diagonally

    return [max_fitting_value, output_seq1, output_seq2]

# similar to global alignment's scoring and local alignment's matrix initialization
def overlap_alignment (seq1, seq2, match_score, mismatch_penalty, indel_penalty):
    dp_mat = numpy.ndarray ((len(seq1)+1, len(seq2)+1))
    backtrack_path = numpy.ndarray ((len(seq1)+1, len(seq2)+1))
    dp_mat[0,0] = 0

    for i in xrange (1, len(seq1)+1):
        dp_mat [i, 0] = 0   # no penalty for indels (gap) at the starting edge of seq1
        backtrack_path [i, 0] = 0
    for j in xrange (1, len(seq2)+1):
        dp_mat [0, j] = 0  # no penalty for indels (gap) at starting edge of seq2
        backtrack_path [0, j] = 1
    for i in xrange (1, len(seq1)+1):
        for j in xrange (1, len(seq2)+1):
            if (seq1[i-1] == seq2[j-1]):
                diag_value = dp_mat[i-1, j-1] + match_score
            else:
                diag_value = dp_mat[i-1, j-1] - mismatch_penalty
            dp_mat [i,j] = max (dp_mat[i-1, j] - indel_penalty, dp_mat[i, j-1] - indel_penalty, diag_value)
            if (dp_mat [i, j] == dp_mat[i-1, j] - indel_penalty):
                backtrack_path [i, j] = 0   #'down'
            elif (dp_mat [i, j] == dp_mat[i, j-1] - indel_penalty):
                backtrack_path [i, j] = 1    #'right'
            else:
                backtrack_path [i, j] = 2   #'diagonal'

    # print dp_mat
    return [backtrack_path, dp_mat]

# backtracks from the max value in the last row of dynamic programming matrix; if multiple, takes max value from the maximum column
def overlap_align_backtrack (backtrack_path, dp_mat, seq1, seq2):
    output_seq1 = ''
    output_seq2 = ''

    column = numpy.argmax(dp_mat[-1,:])   # starting column is the one with max value in the last row
    row = dp_mat.shape[0]-1   # starting row is the last row
    max_overlap_value = dp_mat[row][column]
    while (row != 0 and column != 0):
        # print backtrack_path[row,column]   # prints out backtracking decision sequence
        if (backtrack_path[row,column] == 0):    # down direction, insertion in seq1
            output_seq2 = '-' + output_seq2
            output_seq1 = seq1[row-1] + output_seq1
            row -= 1     # go back by moving up a row
        elif (backtrack_path[row, column] == 1):   # right direction, insertion in seq2
            output_seq1 = '-' + output_seq1
            output_seq2 = seq2[column-1] + output_seq2
            column -= 1
        else:
            output_seq1 = seq1[row-1] + output_seq1
            output_seq2 = seq2[column-1] + output_seq2
            row -= 1
            column -= 1    # go back diagonally

    return [max_overlap_value, output_seq1, output_seq2]    

# assuming match/mismatch score matrix is given
def affine_gap_alignment (seq1, seq2, gap_open_close_penalty, gap_extension_penalty, match_mismatch_scoring_dict):
    middle_dp_mat = numpy.ndarray ((len(seq1)+1, len(seq2)+1))    # defines match/mismatch (diagonal) scoring
    lower_dp_mat = numpy.ndarray ((len(seq1)+1, len(seq2)+1))   # defines deletion scoring
    upper_dp_mat = numpy.ndarray ((len(seq1)+1, len(seq2)+1))   # defines insertion scoring
    backtrack_path_middle = numpy.ndarray ((len(seq1)+1, len(seq2)+1)).astype(str)
    backtrack_path_lower = numpy.ndarray ((len(seq1)+1, len(seq2)+1)).astype(str)
    backtrack_path_upper = numpy.ndarray ((len(seq1)+1, len(seq2)+1)).astype(str)
    middle_dp_mat[0,0] = 0
    lower_dp_mat[0,0] = -gap_open_close_penalty
    lower_dp_mat[0,1] = -9999   # placeholder for (-)infinity
    upper_dp_mat[0,0] = -gap_open_close_penalty
    upper_dp_mat[1,0] = -9999   # placeholder for (-)infinity

    middle_dp_mat[1,0] = middle_dp_mat[0,1] = -gap_open_close_penalty - gap_extension_penalty
    lower_dp_mat[1,0] = -gap_open_close_penalty - gap_extension_penalty
    upper_dp_mat[0,1] = -gap_open_close_penalty - gap_extension_penalty

    backtrack_path_middle[0,0] = '-'
    backtrack_path_lower[0,0] = backtrack_path_lower[0,1] = '-'
    backtrack_path_upper[0,0] = backtrack_path_upper[1,0] = '-'
    backtrack_path_lower[1,0] = backtrack_path_middle[1,0] = 'd'
    backtrack_path_upper[0,1] = backtrack_path_middle[0,1] = 'r'

    for i in xrange (2, len(seq1)+1):
        middle_dp_mat [i, 0] = middle_dp_mat[i-1, 0] - gap_extension_penalty
        lower_dp_mat [i, 0] = lower_dp_mat[i-1, 0] - gap_extension_penalty
        upper_dp_mat [i, 0] = -9999
        backtrack_path_upper[i,0] = '-'
        backtrack_path_lower[i,0] = 'd'
        backtrack_path_middle[i,0] = 'd'
    for j in xrange (2, len(seq2)+1):
        middle_dp_mat [0, j] = middle_dp_mat[0, j-1] - gap_extension_penalty
        lower_dp_mat [0, j] = -9999
        upper_dp_mat [0, j] = upper_dp_mat[0, j-1] - gap_extension_penalty
        backtrack_path_lower[0,j] = '-'
        backtrack_path_upper[0,j] = 'r'
        backtrack_path_middle[0,j] = 'r'
    for i in xrange (1, len(seq1)+1):
        for j in xrange (1, len(seq2)+1):
            lower_dp_mat[i,j] = max (lower_dp_mat[i-1,j] - gap_extension_penalty, middle_dp_mat[i-1,j] - gap_open_close_penalty)
            upper_dp_mat[i,j] = max (upper_dp_mat[i,j-1] - gap_extension_penalty, middle_dp_mat[i,j-1] - gap_open_close_penalty)
            if (seq1[i-1] == seq2[j-1]):
                middle_mat_diag_value = middle_dp_mat[i-1, j-1] + float(match_mismatch_scoring_dict[seq1[i-1]][seq1[i-1]])
            else:
                middle_mat_diag_value = middle_dp_mat[i-1, j-1] + float(match_mismatch_scoring_dict[seq1[i-1]][seq2[j-1]])
            middle_dp_mat [i, j] = max (lower_dp_mat[i,j], upper_dp_mat[i,j], middle_mat_diag_value)

            if (lower_dp_mat[i,j] == lower_dp_mat[i-1,j] - gap_extension_penalty):
                backtrack_path_lower[i,j] = 'd'
            else:
                backtrack_path_lower[i,j] = 'm'

            if (upper_dp_mat[i,j] == upper_dp_mat[i,j-1] - gap_extension_penalty):
                backtrack_path_upper[i,j] = 'r'
            else:
                backtrack_path_upper[i,j] = 'm'

            # order does matter here, scoring matrix differs by little with different condition orders
            if (middle_dp_mat[i,j] == middle_mat_diag_value):
                backtrack_path_middle[i,j] = '\\'
            elif (middle_dp_mat[i,j] == lower_dp_mat[i,j]):
                backtrack_path_middle[i,j] = 'd'
            elif (middle_dp_mat[i,j] == upper_dp_mat[i,j]):
                backtrack_path_middle[i,j] = 'r'

    # print 'lower: ' + '\n' + str(lower_dp_mat.astype(int))
    # print 'upper: ' + '\n' + str(upper_dp_mat.astype(int))
    # print 'middle: ' + '\n' + str(middle_dp_mat.astype(int))
    # print backtrack_path_middle
    # print backtrack_path_lower
    # print backtrack_path_upper
    dp_mat_max_values = [lower_dp_mat[len(seq1), len(seq2)], middle_dp_mat[len(seq1), len(seq2)], upper_dp_mat[len(seq1), len(seq2)]]
    max_value = max(dp_mat_max_values)
    max_mat = dp_mat_max_values.index(max_value)
    return [max_value, max_mat, backtrack_path_lower, backtrack_path_middle, backtrack_path_upper]

def affine_gap_align_backtrack3 (seq1, seq2, max_mat, backtrack_path_lower, backtrack_path_middle, backtrack_path_upper):
    output_seq1 = ''
    output_seq2 = ''

    # for global alignment only
    row = len(seq1)
    column = len(seq2)
    which_mat = max_mat   # 0 = lower, 1 = middle, 2 = upper

    while (row != 0 or column != 0):
        if (which_mat == 1):
            if (backtrack_path_middle[row][column] == 'd'):
                which_mat = 0
            elif (backtrack_path_middle[row][column] == 'r'):
                which_mat = 2
            else:           # moves diagonally back
                output_seq1 = seq1[row-1] + output_seq1
                output_seq2 = seq2[column-1] + output_seq2
                row -= 1
                column -= 1

        elif (which_mat == 0):
            if (backtrack_path_lower[row][column] == 'm'):
                output_seq1 = seq1[row-1] + output_seq1
                output_seq2 = '-' + output_seq2
                row -= 1
                which_mat = 1
            else:
                output_seq1 = seq1[row-1] + output_seq1
                output_seq2 = '-' + output_seq2
                row -= 1

        elif (which_mat == 2):
            if (backtrack_path_upper[row][column] == 'm'):
                output_seq1 = '-' + output_seq1
                output_seq2 = seq2[column-1] + output_seq2
                column -= 1
                which_mat = 1
            else:
                output_seq1 = '-' + output_seq1
                output_seq2 = seq2[column-1] + output_seq2
                column -= 1

    return [output_seq1, output_seq2]

# similar to global alignment, but with 3 dimensional dynamic programming matrix
# NOTE : indel favoring, as they are put in priority in case of a tie in max score
def multiple_sequence_alignment (seq1, seq2, seq3, match_score, indel_penalty, mismatch_penalty):
    dp_mat = numpy.zeros ((len(seq1)+1, len(seq2)+1, len(seq3)+1))
    backtrack_path = numpy.zeros ((len(seq1)+1, len(seq2)+1, len(seq3)+1))
    # dp_mat[0,0,0] = 0

    # dp matrix and backtrack path initialization
    for i in xrange (1, len(seq1)+1):
        dp_mat [i, 0, 0] = dp_mat [i-1, 0, 0] - indel_penalty
        backtrack_path [i, 0, 0] = 0   # associated with corner 1
        for j in xrange(1, len(seq2)+1):
            backtrack_path [i, j, 0] = 3   # associated with corner 4
        for k in xrange(1, len(seq3)+1):
            backtrack_path [i, 0, k] = 4   # associated with corner 5
    for j in xrange (1, len(seq2)+1):
        dp_mat [0, j, 0] = dp_mat [0, j-1, 0] - indel_penalty
        backtrack_path [0, j, 0] = 1   # associated with corner 2
        for k in xrange (1, len(seq3)+1):
            backtrack_path [0, j, k] = 5   # associated with corner 6
    for k in xrange (1, len(seq3)+1):
        dp_mat [0, 0, k] = dp_mat [0, 0, k-1] - indel_penalty
        backtrack_path [0, 0, k] = 2   # associated with corner 3

    # assign scores to non-initialized dp matrix elements
    for i in xrange (1, len(seq1)+1):
        for j in xrange (1, len(seq2)+1):
            for k in xrange (1, len(seq3)+1):
                # seq1[i-1] seq2[j-1] seq3[k-1] are the nucleotides that the alignment is currently on,
                # while backtrack_path[i,j,k] and dp_mat[i,j,k] are the backtrack/dp_mat matrix locations currently on
                # imagine the 7 corners of a cube going to the 8th corner
                # corner_1
                corner_1 = dp_mat[i-1,j,k] - indel_penalty
                # corner 2
                corner_2 = dp_mat[i,j-1,k] - indel_penalty
                # corner 3
                corner_3 = dp_mat[i,j,k-1] - indel_penalty
                # corner 4
                corner_4 = dp_mat[i-1,j-1,k] - indel_penalty
                # corner 5
                corner_5 = dp_mat[i-1,j,k-1] - indel_penalty
                # corner 6
                corner_6 = dp_mat[i,j-1,k-1] - indel_penalty
                # corner 7
                if (seq1[i-1] == seq2[j-1] and seq1[i-1] == seq3[k-1]):
                    corner_7 = dp_mat[i-1, j-1, k-1] + match_score
                    # print 'matched at: ' + seq1[i-1]
                    # print 'matched index : ' + str(i-1) + ' , ' + str(j-1) + ' , ' + str(k-1)
                    # print 'stored at: ' + str(i) + ' , ' + str(j) + ' , ' + str(k)
                else:
                    corner_7 = dp_mat[i-1, j-1, k-1] - mismatch_penalty

                corners = [corner_1,corner_2,corner_3,corner_4,corner_5,corner_6,corner_7]
                dp_mat[i,j,k] = max(corners)
                max_ind = corners.index(dp_mat[i,j,k])
                backtrack_path[i,j,k] = max_ind
    # print dp_mat
    # print backtrack_path
    max_value = dp_mat[len(seq1), len(seq2), len(seq3)]
    return [max_value, backtrack_path]

# assume that dim1_init, dim2_init, and dim3_init contain the starting location for backtrack
def multiple_sequence_align_backtrack (backtrack_path, seq1, seq2, seq3, dim1_init, dim2_init, dim3_init):
    dim1 = dim1_init
    dim2 = dim2_init
    dim3 = dim3_init
    output_seq1 = ''
    output_seq2 = ''
    output_seq3 = ''

    # print backtrack_path
    while (dim1 != 0 or dim2 or 0 or dim3 != 0):
        if (backtrack_path[dim1, dim2, dim3] == 0):    # corner 1 direction
            output_seq1 = seq1[dim1-1] + output_seq1
            output_seq2 = '-' + output_seq2
            output_seq3 = '-' + output_seq3
            dim1 -= 1   # go to corner 1
        elif (backtrack_path[dim1, dim2, dim3] == 1):    # corner 2 direction
            output_seq1 = '-' + output_seq1
            output_seq2 = seq2[dim2-1] + output_seq2
            output_seq3 = '-' + output_seq3
            dim2 -= 1  # go to corner 2
        elif (backtrack_path[dim1, dim2, dim3] == 2):    # corner 3 direction
            output_seq1 = '-' + output_seq1
            output_seq2 = '-' + output_seq2
            output_seq3 = seq3[dim3-1] + output_seq3
            dim3 -= 1   # go to corner 3
        elif (backtrack_path[dim1, dim2, dim3] == 3):    # corner 4 direction
            output_seq1 = seq1[dim1-1] + output_seq1
            output_seq2 = seq2[dim2-1] + output_seq2
            output_seq3 = '-' + output_seq3
            dim1 -= 1   # go to corner 4
            dim2 -= 1    # go to corner 4
        elif (backtrack_path[dim1, dim2, dim3] == 4):    # corner 5 direction
            output_seq1 = seq1[dim1-1] + output_seq1
            output_seq2 = '-' + output_seq2
            output_seq3 = seq3[dim3-1] + output_seq3
            dim1 -= 1   # go to corner 5
            dim3 -= 1    # go to corner 5
        elif (backtrack_path[dim1, dim2, dim3] == 5):    # corner 6 direction
            output_seq1 = '-' + output_seq1
            output_seq2 = seq2[dim2-1] + output_seq2
            output_seq3 = seq3[dim3-1] + output_seq3
            dim2 -= 1   # go to corner 6
            dim3 -= 1   # go to corner 6
        elif (backtrack_path[dim1, dim2, dim3] == 6):    # corner 7 direction
            output_seq1 = seq1[dim1-1] + output_seq1
            output_seq2 = seq2[dim2-1] + output_seq2
            output_seq3 = seq3[dim3-1] + output_seq3
            dim1 -= 1   # go to corner 7
            dim2 -= 1   # go to corner 7
            dim3 -= 1    # go to corner 7

    return [output_seq1, output_seq2, output_seq3]

# re-use global alignment code to handle this special case?
def middle_edge_score (seq1, seq2, indel_penalty, match_mismatch_scoring_dict):
    middle_column = len(seq2)/2
    dp_mat = numpy.ndarray ((len(seq1)+1, middle_column+1))
    dp_mat[0,0] = 0

    for i in xrange (1, len(seq1)+1):
        dp_mat [i, 0] = dp_mat[i-1, 0] - indel_penalty
    for j in xrange (1, middle_column+1):
        dp_mat [0, j] = dp_mat[0, j-1] - indel_penalty
    for i in xrange (1, len(seq1)+1):
        for j in xrange (1, middle_column+1):
            if (seq1[i-1] == seq2[j-1]):
                diag_value = dp_mat[i-1, j-1] + float(match_mismatch_scoring_dict[seq1[i-1]][seq1[i-1]])
            else:
                diag_value = dp_mat[i-1, j-1] + float(match_mismatch_scoring_dict[seq1[i-1]][seq2[j-1]])
            dp_mat [i, j] = max (dp_mat[i-1, j] - indel_penalty, dp_mat[i, j-1] - indel_penalty, diag_value)
    # print dp_mat.astype(int)
    side1_middle_column_scores = dp_mat[:,-1].astype(int)

    side2_middle_column = len(seq2) - middle_column
    dp_mat2 = numpy.ndarray ((len(seq1)+1, side2_middle_column+1))
    backtrack_path = numpy.ndarray ((len(seq1)+1, side2_middle_column+1))
    seq1_reversed = seq1[::-1]
    seq2_reversed = seq2[::-1]
    for i in xrange (1, len(seq1)+1):
        dp_mat2 [i, 0] = dp_mat[i-1, 0] - indel_penalty
        backtrack_path [i, 0] = 0
    for j in xrange (1, side2_middle_column+1):
        dp_mat2 [0, j] = dp_mat[0, j-1] - indel_penalty
        backtrack_path [0, j] = 1
    for i in xrange (1, len(seq1)+1):
        for j in xrange (1, side2_middle_column+1):
            if (seq1_reversed[i-1] == seq2_reversed[j-1]):
                diag_value = dp_mat2[i-1, j-1] + float(match_mismatch_scoring_dict[seq1_reversed[i-1]][seq1_reversed[i-1]])
            else:
                diag_value = dp_mat2[i-1, j-1] + float(match_mismatch_scoring_dict[seq1_reversed[i-1]][seq2_reversed[j-1]])
            dp_mat2 [i, j] = max (dp_mat2[i-1, j] - indel_penalty, dp_mat2[i, j-1] - indel_penalty, diag_value)
            if (dp_mat2 [i, j] == dp_mat2[i-1, j]- indel_penalty):
                backtrack_path [i, j] = 0   #'down'
            elif (dp_mat2 [i, j] == dp_mat2[i, j-1] - indel_penalty):
                backtrack_path [i, j] = 1    #'right'
            else:
                backtrack_path [i, j] = 2   #'diagonal'

    # print dp_mat2.astype(int)
    # print backtrack_path.astype(int)
    side2_middle_column_scores = dp_mat2[:,-1][::-1]
    middle_column_score_combined = []
    for i in xrange(side1_middle_column_scores.size):
        middle_column_score_combined.append(side1_middle_column_scores[i] + side2_middle_column_scores[i])
    max_score_index = middle_column_score_combined.index(max(middle_column_score_combined))
    source_node = [max_score_index, middle_column]
    
    sink_node_direction = backtrack_path[:,-1][::-1][max_score_index]  # takes the reverse of backtrack path's last column, find path going to source node
    if (sink_node_direction == 2):
        sink_node = [source_node[0]+1, source_node[1]+1]
    elif (sink_node_direction == 1):
        sink_node = [source_node[0], source_node[1]+1]
    elif (sink_node_direction == 0):
        sink_node = [source_node[0]+1, source_node[1]]
    else:
        print 'this should not happen'
    return [source_node, sink_node]

# recursively finds the middle column edges to build best global alignment
def linear_space_alignment (seq1, seq2, indel_penalty, match_mismatch_scoring_dict):
    middle_node_column = len(seq2)/2
    if (len(seq2) == 0):
        return [seq1,'']
    if (len(seq1) == 0):
        return ['', seq2]
    [source_node, sink_node] = middle_edge_score (seq1, seq2, indel_penalty, match_mismatch_scoring_dict)
    # print 'middle edge source node: ' + str(source_node) + ' , middle edge sink node: ' + str(sink_node)
    # print 'middle seq1 : ' + seq1[source_node[0]:sink_node[0]] + ' , middle seq2: ' + seq2[source_node[1]:sink_node[1]]
    middle_node_row = source_node[0]
    seq1_substr = seq1[source_node[0]:sink_node[0]]
    seq2_substr = seq2[source_node[1]:sink_node[1]]
    
    # for indels in sequences
    if (len(seq1_substr) == 0):
        seq1_substr = '-'
    if (len(seq2_substr) == 0):
        seq2_substr = '-'

    [left_seq1, left_seq2] = linear_space_alignment (seq1[:middle_node_row], seq2[:middle_node_column], indel_penalty, match_mismatch_scoring_dict)   # recurse through smaller rectangles (top left hand side)
    seq1_substr = left_seq1 + seq1_substr
    seq2_substr = left_seq2 + seq2_substr

    if (sink_node[1]>source_node[1]):  # horizontal or diagonal middle edge case
        middle_node_column += 1
    if (sink_node[0]>source_node[0]):   # vertical or diagonal middle edge case
        middle_node_row += 1

    [right_seq1, right_seq2] = linear_space_alignment (seq1[middle_node_row:], seq2[middle_node_column:], indel_penalty, match_mismatch_scoring_dict)   # recurse through smaller rectangles (bottom right hand side)
    seq1_substr += right_seq1
    seq2_substr += right_seq2

    return [seq1_substr, seq2_substr]

def reconstruct_score (seq1, seq2, indel_penalty, match_mismatch_scoring_dict):
    if (len(seq1) != len(seq2)):
        assert 'Length of scoring sequences not equal'

    score = 0
    for i in xrange (len(seq1)):
        if (seq1[i] != '-' and seq2[i] != '-'):
            score += int(match_mismatch_scoring_dict[seq1[i]][seq2[i]])
        else:
            score -= indel_penalty

    return score
