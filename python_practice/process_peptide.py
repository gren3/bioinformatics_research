#!/usr/bin/env python

import sys
import argparse
import re
import string
import collections   #defaultdict
from collections import Counter
from operator import add
import itertools

class Peptide(object):
    def __init__(self, amino_acid_string):
        self.seq = amino_acid_string.strip()
        self.peptide_weight_table = {'G':57,'A':71,'S':87,'P':97,'V':99,
            'T':101,'C':103,'I':113,'L':113,
            'N':114,'D':115,'K':128,'Q':128,
            'E':129,'M':131,'H':137,'F':147,
            'R':156,'Y':163,'W':186}

    def hamming_distance (self, seq1, seq2):
        assert len(seq1) == len(seq2)
        hamm_dist = 0
        for i in xrange(len(seq1)):
            if (seq1[i] != seq2[i]):
                hamm_dist += 1

        return hamm_dist

    def rna_to_amino_acid (self, rna_seq):
        codon_table = {'UUU':'F', 'UUC':'F', 'UUA':'L', 'UUG':'L',
                    'UCU':'S', 'UCC':'S', 'UCA':'S', 'UCG':'S',
                    'UAU':'Y', 'UAC':'Y', 'UAA':'STOP', 'UAG':'STOP',
                    'UGU':'C', 'UGC':'C', 'UGA':'STOP', 'UGG':'W',
                    'CUU':'L', 'CUC':'L', 'CUA':'L', 'CUG':'L',
                    'CCU':'P', 'CCC':'P', 'CCA':'P', 'CCG':'P',
                    'CAU':'H', 'CAC':'H', 'CAA':'Q', 'CAG':'Q',
                    'CGU':'R', 'CGC':'R', 'CGA':'R', 'CGG':'R',
                    'AUU':'I', 'AUC':'I', 'AUA':'I', 'AUG':'M',
                    'ACU':'T', 'ACC':'T', 'ACA':'T', 'ACG':'T',
                    'AAU':'N', 'AAC':'N', 'AAA':'K', 'AAG':'K',
                    'AGU':'S', 'AGC':'S', 'AGA':'R', 'AGG':'R',
                    'GUU':'V', 'GUC':'V', 'GUA':'V', 'GUG':'V',
                    'GCU':'A', 'GCC':'A', 'GCA':'A', 'GCG':'A',
                    'GAU':'D', 'GAC':'D', 'GAA':'E', 'GAG':'E',
                    'GGU':'G', 'GGC':'G', 'GGA':'G', 'GGG':'G'}

        ind = 0
        amino_acid_seq = ''
        while (ind <= len(rna_seq) - 3):
            if (codon_table[rna_seq[ind:ind+3]] != 'STOP'):
                amino_acid_seq = amino_acid_seq + codon_table[rna_seq[ind:ind+3]]
                ind = ind + 3
            else:
                break
        return amino_acid_seq

    def match_peptide (self, peptide_string):
        kmer_length = len(peptide_string)*3
        kmer_dict = self.build_kmer_dict (self.seq, kmer_length)  # build kmer dictionary of given dna sequence
        matched_seq = []
        for key in kmer_dict:
            # print key + ' ' + self.rna_to_amino_acid(self.dna_to_rna(key))
            if (self.rna_to_amino_acid(self.dna_to_rna(key)) == peptide_string):
                matched_seq.append (key)
            elif (self.rna_to_amino_acid(self.dna_to_rna(self.reverse_complement(key))) == peptide_string):
                matched_seq.append (key)
        return matched_seq

    def dna_to_rna (self, dna_string):
        return dna_string.upper().replace('T','U')

    def combination_all_sizes (self, seq):
        combos = []
        for i in xrange(len(seq)+1):
            combos.extend(list(itertools.combinations(seq, i)))
        return combos

    def peptide_to_weight (self, peptide):
        weight = 0
        for amino_acid in peptide:  # assuming a peptide is represented by a tuple of amino acid letters
            weight += self.peptide_weight_table[amino_acid]
        return weight

    # original peptide is a string of amino acid letters, cyclic
    def generate_subpeptides (self, original_peptide):
        subpeptides = []
        subpeptides.append ('')  # subpeptide of 0 length
        subpeptides.append (original_peptide)  # subpeptide of full length
        original_peptide_2x = original_peptide*2   # concatenate same strings together

        for subpeptide_length in xrange(1,len(original_peptide)):
            index = 0
            while (index < len (original_peptide)):
                if (index + subpeptide_length > len (original_peptide)):
                    subpeptides.append (original_peptide_2x[index:index+subpeptide_length])
                    # print original_peptide_2x[index:index+subpeptide_length]
                else:
                    subpeptides.append (original_peptide[index:index+subpeptide_length])
                    # print original_peptide[index:index+subpeptide_length]
                index += 1
        return subpeptides

    def generate_subpeptides_weights (self, original_peptide_weights):
        subpeptide_weights = [0]  # subpeptide of 0 length
        subpeptide_weights.append (sum(original_peptide_weights))  # subpeptide of full length
        original_peptide_weights_2x = original_peptide_weights*2   # concatenate same strings together

        for subpeptide_length in xrange(1,len(original_peptide_weights)):
            index = 0
            while (index < len (original_peptide_weights)):
                if (index + subpeptide_length > len (original_peptide_weights)):
                    subpeptide_weights.append (sum(original_peptide_weights_2x[index:index+subpeptide_length]))
                else:
                    subpeptide_weights.append (sum(original_peptide_weights[index:index+subpeptide_length]))
                index += 1
        return subpeptide_weights

    # takes in only a string of amino acid letters for peptide, cyclic
    def generate_peptide_mass_spec (self, peptide):
        subpeptides = self.generate_subpeptides (peptide)

        weight_list = []
        for subpeptide in subpeptides:  # peptide combinations of length 0 to len(initial_peptide)
            weight_list.append (self.peptide_to_weight (subpeptide))
        return sorted(weight_list)

    # takes a list of weights of amino acids for peptide, cyclic
    def generate_peptide_mass_spec_weights (self, peptide_weights):
        subpeptide_weights = self.generate_subpeptides_weights (peptide_weights)

        weight_list = []
        for weight in subpeptide_weights:  # peptide combinations of length 0 to len(initial_peptide)
            weight_list.append (weight)
        return sorted(weight_list)

    # takes in a list of lists, returns the list of lists with 18 different amino acid weights appended to each list
    def extend_peptide_weight (self, start_list):
        extended_list = []
        
        # for each existing peptide sequence in starting list
        for peptide in start_list:
            distinct_weights = list(set(self.peptide_weight_table.values()))  # list of 18 distinct amino acid weights
            extended_list.extend ([peptide + [x] for x in distinct_weights])  # adds the 18 weights to peptide generating 18 new sequences
        return extended_list

    # assumes aa list contains the weights of amino acids used to build peptide    
    def extend_peptide_weight_aa_restricted (self, start_list, aa_list):
        extended_list = []
        
        # for each existing peptide sequence in starting list
        for peptide in start_list:
            extended_list.extend ([peptide + [x] for x in aa_list])
        return extended_list

    # returns if list1 contains all elements within list2, list elements must be of same type
    def list_within_list (self, list1, list2):
        list1 = sorted (list1)
        list2 = sorted (list2)

        # print list2
        for i in xrange(len(list2)):
            if (list2[i] in list1):
                list1.remove(list2[i])
            else:
                # print str(list2) + 'False'
                return False
        return True

    # returns a string of amino acid letters when given their weights, for amino acids sharing same weight returns first
    def peptide_weight_to_string (self, peptide_weights):
        peptide_string = []

        for search_weight in peptide_weights:
            for amino_acid, weight in self.peptide_weight_table.iteritems():
                if weight == search_weight:
                    peptide_string.append (amino_acid)
                    break
        return ''.join (peptide_string)

    # non-cyclic, use with cyclopeptide reconstruction
    def generate_subpeptides2 (self, original_peptide):
        subpeptides = []
        subpeptides.append ('')  # subpeptide of 0 length
        subpeptides.append (original_peptide)  # subpeptide of full length
        
        for subpeptide_length in xrange(1,len(original_peptide)):
            index = 0
            while (index < len (original_peptide) - subpeptide_length + 1):
                subpeptides.append (original_peptide[index:index+subpeptide_length])
                index += 1
        return subpeptides
    
    # non-cyclic version, use with cyclopeptide reconstruction
    def generate_peptide_mass_spec2 (self, peptide):
        subpeptides = self.generate_subpeptides2 (peptide)

        weight_list = []
        for subpeptide in subpeptides:  # peptide combinations of length 0 to len(initial_peptide)
            weight_list.append (self.peptide_to_weight (subpeptide))
        return sorted(weight_list)

    # spectrum is a list of spectrum weights
    def cyclopeptide_sequencing (self, spectrum):
        spectrum = [int(x) for x in spectrum]  # converts list of strings to ints
        peptide_list = [[]]
        return_list = []

        while (len(peptide_list) > 0):
            temp_list = []
            return_list = peptide_list
            # print return_list
            peptide_list = self.extend_peptide_weight (peptide_list)
            for peptide in peptide_list:
                generated_spectrum = self.generate_peptide_mass_spec2 (self.peptide_weight_to_string (peptide))
                if (self.list_within_list (spectrum, generated_spectrum)):
                    # print peptide
                    temp_list.append (peptide)  # if peptide's weights are consistent with spectrum, save it
            peptide_list = temp_list
        return return_list

    # similar to finding a list within a list of weights, spectrums compared are cyclic
    def score (self, peptide, spectrum):
        # peptide_spectrum = self.generate_peptide_mass_spec (self.peptide_weight_to_string (peptide))
        peptide_spectrum = self.generate_peptide_mass_spec_weights (peptide)
        list1 = sorted (spectrum)
        list2 = sorted (peptide_spectrum)
        score = 0

        for i in xrange(len(list2)):
            if (list2[i] in list1):
                list1.remove(list2[i])
                score += 1
        return score

    # trims ranking list to 'cut_off_num' number of highest score peptides
    def update_top_scorer (self, scorer_dict, new_peptide, spectrum, cut_off_num):
        peptide_score = self.score (new_peptide, spectrum)
        distinct_scores = set(scorer_dict.values())

        # print str(new_peptide) + str(peptide_score)
        # adds new peptide and its score to score dictionary
        scorer_dict ['-'.join([str(weight) for weight in new_peptide])] = peptide_score

        # check if number of top scorers tracked is greater than 'cut_off_num'
        if (len(scorer_dict) > cut_off_num):
            ranked_scores = sorted(list(set(scorer_dict.values())))
            # remove peptides in dictionary with lower score
            new_dict = {key: val for key, val in scorer_dict.items() if val != ranked_scores[0]}
            # if after removal of lowest score, cut off num is better met, keep new dictionary, else keep original
            if (len(new_dict) >= cut_off_num):
                scorer_dict = new_dict

        # print scorer_dict
        return scorer_dict

    def return_max_scorer_list (self, scorer_dict):
        return_list = []

        ranked_scores = sorted(list(set(scorer_dict.values())))
        max_scorer_list_peptides = [key.split('-') for key, val in scorer_dict.items() if val == ranked_scores[-1]]
        for peptide in max_scorer_list_peptides:
            return_list.append ([int(weight) for weight in peptide])
        return return_list

    # assuming spectrum is a list of integer weights
    def ranked_cyclopeptide_sequencing (self, spectrum, cut_off_num):
        true_total_weight = max(spectrum)
        peptide_list = [[]]
        return_dict = {}
        temp_list = []
        score_dict = {}
        iteration = 1
        cut_score_of_round = 0
        best_score_of_round = 0
        cur_high_score = 0
        top_scorer_list = []
        size_before_cut = 0

        while (len(peptide_list) > 0):
            print 'round: ' + str(iteration) + ' leaderboard size: ' + str(len (peptide_list)) + ' cut score : ' + str (cut_score_of_round) + ' best score : ' + str(best_score_of_round) + ' size before cut: ' + str(size_before_cut)
            iteration += 1
            temp_list = []
            return_dict = score_dict
            score_dict = {}
            peptide_list = self.extend_peptide_weight (peptide_list)
            size_before_cut = 0

            for peptide in peptide_list:
                if (sum(peptide) <= true_total_weight):
                    peptide_score = self.score (peptide, spectrum)
                    if (sum(peptide) == true_total_weight):
                        if (peptide_score > cur_high_score):
                            cur_high_score = peptide_score
                            top_scorer_list = [peptide]
                        elif (peptide_score == cur_high_score):
                            top_scorer_list.append (peptide)
                    score_dict = self.update_top_scorer (score_dict, peptide, spectrum, cut_off_num)
                    size_before_cut += 1
                    # print str(peptide) + ' score : ' + str(peptide_score)
                    # print score_dict
            if (len(score_dict.values()) > 0):
                cut_score_of_round = min (score_dict.values())
                best_score_of_round = max (score_dict.values())
            for key, val in score_dict.items():
                # print key +' '+str(val)
                peptide = key.split('-')
                peptide = [int(weight) for weight in peptide]   # recovering int list from string
                temp_list.append (peptide)
            peptide_list = temp_list
        # return self.return_max_scorer_list (return_dict)
        return top_scorer_list

    # assuming spectrum is a list of integer weights
    def spectral_convolution (self, spectrum):
        spectrum = sorted (spectrum)
        convolution_results = []

        for i in xrange (1, len(spectrum)):
            for j in xrange (len(spectrum)):
                diff = spectrum[i] - spectrum[j] 
                if (diff > 0):
                    convolution_results.append (diff)
        return convolution_results

    # amino acid available for peptide reconstruction limited to list returned from spectral convolution
    def ranked_cyclopeptide_sequencing_aa_restricted (self, spectrum, cut_off_num, aa_list):
        true_total_weight = max(spectrum)
        peptide_list = [[]]
        return_dict = {}
        temp_list = []
        score_dict = {}
        iteration = 1
        cut_score = 0
        prev_max_score = 0

        while (len(peptide_list) > 0):
            # print 'round: ' + str(iteration) + ' leaderboard size: ' + str(len (peptide_list)) + ' score : ' + str (cut_score)
            iteration += 1
            temp_list = []
            return_dict = score_dict
            score_dict = {}
            # print return_list
            peptide_list = self.extend_peptide_weight_aa_restricted (peptide_list, aa_list)
            for peptide in peptide_list:
                if (sum(peptide) <= true_total_weight):
                    peptide_score = self.score (peptide, spectrum)
                    # print str(peptide) + ' score : ' + str(peptide_score)
                    score_dict = self.update_top_scorer (score_dict, peptide, spectrum, cut_off_num)
            if (len(score_dict) > 0):
                cut_score = min (score_dict.values())
            for key, val in score_dict.items():
                if (prev_max_score >= max (score_dict.values ())):
                    temp_list = []
                    break
                # print key +' '+str(val)
                peptide = key.split('-')
                peptide = [int(weight) for weight in peptide]   # recovering int list from string
                temp_list.append (peptide)
            if (len(temp_list) > 0):
                prev_max_score = max (score_dict.values())
            peptide_list = temp_list
        return self.return_max_scorer_list (return_dict)

    def ranked_convolution_cyclopeptide_sequencing (self, spectrum, cut_off_num_conv, cut_off_num_seq):
        convolution_weights = self.spectral_convolution (spectrum)
        convolution_counts = collections.defaultdict(int)

        for convolution_weight in convolution_weights:
            if (convolution_weight >= 57 and convolution_weight <= 200):
                convolution_counts [convolution_weight] += 1

        ranked_convolution_counts = sorted (list(convolution_counts.values()))
        # for key,val in convolution_counts.items():
        #     print str(key) + ' ' + str(val)

        if (len(ranked_convolution_counts) > cut_off_num_conv):
            cut_off_conv_occurrence = ranked_convolution_counts [-cut_off_num_conv]
            # selects the top 'cut off number' of convolution values
            convolution_weights = [key for key, val in convolution_counts.items() if (val >= cut_off_conv_occurrence)]
        else:
            convolution_weights = list(set(convolution_counts.keys()))

        # print convolution_weights
        return self.ranked_cyclopeptide_sequencing_aa_restricted (spectrum, cut_off_num_seq, convolution_weights)
