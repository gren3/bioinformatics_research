#!/usr/bin/env python

import sys
import argparse
import re
import string
import collections   #defaultdict
import time

from process_dna import * #assuming same directory

if __name__ == "__main__":

	parser = argparse.ArgumentParser(description = 'Bioinformatics python practice')
	parser.add_argument('--input_path', metavar='path', type=str, help='Input file path')
	parser.add_argument('--kmer_length', metavar='int', type=int, help='Kmer length of interest')
	parser.add_argument('--command', metavar='string', type=str, help='Enter what to do to input')
	parser.add_argument('--input_pattern', metavar='string', type=str, help='Enter a pattern to match within DNA')
	parser.add_argument('--clump_length', metavar='int', type=int, help='Length of clumps to look at')
	parser.add_argument('--kmer_occ', metavar='int', type=int, help='Number of kmer occurrences expected')
	parser.add_argument('--hamm_bound', metavar='int', type=int, help='Hamming distance allowed from pattern')
	args = parser.parse_args()

	textfile = open(args.input_path,'r')
	text = textfile.read()
	my_dna = DNA (text)

	if (args.command == 'most_freq_kmer'):
		kmers = my_dna.find_common_kmer()
		output_str = ''
		for kmer in kmers:
			output_str = output_str + kmer + ' '
	elif (args.command == 'rev_comp'):
		output_str = my_dna.reverse_complement()
	elif (args.command == 'pattern_match'):
		output_list = my_dna.pattern_match (args.input_pattern)
		output_str = ''
		for output in output_list:
			output_str = output_str + str(output) + ' '
	elif (args.command == 'clump_find'):
		clump_dict = my_dna.clump_find (args.clump_length,args.kmer_length,args.kmer_occ)
		output_str = ''
		for key in clump_dict:
			output_str = output_str + key + ' '
	elif (args.command == 'gc_skew'):
		skew_list = my_dna.build_gc_skew_list (my_dna.seq)
		min_val = min (skew_list)
		output_list = [ind for ind, val in enumerate(skew_list) if val == min_val]
		output_str = ''
		for output in output_list:
			output_str = output_str + str(output) + ' '
	elif (args.command == 'mismatch_hamming'):
		kmer_dict = my_dna.build_kmer_dict_indexed (args.kmer_length)
		match_list = []
		for key in kmer_dict:
			if (my_dna.hamming_distance(key,args.input_pattern) <= args.hamm_bound):
				match_list.extend(kmer_dict[key])
		output_str = ''
		match_list = sorted(match_list)
		for i in match_list:
			output_str = output_str + str(i) + ' '
	elif (args.command == 'most_freq_kmer_w_mismatch'):
		# old method runs too slow, had to process all possible kmer combinations
		# kmers = my_dna.build_kmer_dict_w_mismatch_old (my_dna.seq, args.kmer_length, args.hamm_bound)
		kmers = my_dna.find_common_kmer_w_mismatch (my_dna.seq, args.kmer_length, args.hamm_bound)
		output_str = ''
		for kmer in kmers:
			output_str = output_str + kmer + ' '

		# kmer = 'CCCGATCACT'
		# ind = my_dna.check_kmer_w_mismatch (my_dna.seq, kmer, args.hamm_bound)
		# for i in ind:
		# 	output_str = output_str + str(i) + ' '
	elif (args.command == 'most_freq_kmer_w_mismatch_revcomp'):
		start_time = time.time ()
		kmers = my_dna.find_common_kmer_w_mismatch_revcomp (my_dna.seq, args.kmer_length, args.hamm_bound)
		end_time = time.time ()
		elapsed_time = end_time - start_time
		print 'Elapsed Time : ' + str(elapsed_time)

		output_str = ''
		for kmer in kmers:
			output_str = output_str + kmer + ' '
	elif (args.command == 'rna_to_amino_acid'):
		output_str = my_dna.rna_to_amino_acid ()
	elif (args.command == 'match_peptide'):
		output_list = my_dna.match_peptide (args.input_pattern)
		output_str = ''
		for output in output_list:
			output_str = output_str + output + '\n'
	elif (args.command == 'peptide_weight'):
		weight_list = my_dna.peptide_combos_weight (text)
		output_str = ''
		for weight in weight_list:
			output_str = output_str + str(weight) + ' '
	print output_str
