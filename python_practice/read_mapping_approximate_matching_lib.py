#!/usr/bin/env python

import sys
import collections   #defaultdict
from collections import Counter
import itertools
import os
import math

class Trie_Node (object):
	# saves parent node instance, own value, and initializes an empty list of destination nodes
	def __init__(self, parent, value, node_id):
		self.value = value
		self.child = {}   # using a dict, so can search child nodes by value
		self.parent = parent
		self.node_id = node_id   # node count basically

class Trie (object):
	# initializing suffix trie, can take up to 1 initial string
	def __init__(self, init_string):
		self.root = Trie_Node (None, None, 1)
		self.max_node_count = 2   # root node is actually node 1, although it stores no actual value
		if (len(init_string) > 0):
			cur_node = self.root
			for i in xrange (len (init_string)):
				cur_val = init_string[i]
				cur_node.child[cur_val] = Trie_Node (cur_node, cur_val, self.max_node_count)
				cur_node = cur_node.child[cur_val]
				self.max_node_count += 1

	# adds the given string into the existing trie
	def append (self, input_string):
		cur_node = self.root

		# goes down a matching path, if value does not match, start new branch
		string_ind = 0
		while (string_ind < len(input_string)):
			cur_val = input_string[string_ind]
			
			#look for match below
			if cur_val in cur_node.child:
				cur_node = cur_node.child[cur_val]
			else:
				cur_node.child[cur_val] = Trie_Node (cur_node, cur_val, self.max_node_count)
				cur_node = cur_node.child[cur_val]
				self.max_node_count += 1
				# print 'input string: ' + input_string + ' , new branch element: ' + cur_val
			string_ind += 1

	# DFS implemented with python stack, may be used to print out node relations
	def depth_first_traversal (self):
		stack = []    # a stack of nodes

		for key in self.root.child:
			stack.append (self.root.child[key])   # push all child nodes onto stack

		while (len(stack) > 0):
			cur_node = stack.pop ()
			print str(cur_node.parent.node_id) + ' ' + str(cur_node.node_id) + ' ' + cur_node.value
			for key in cur_node.child:
				stack.append (cur_node.child[key])

# assuming string_list is a list of strings used to construct trie
def construct_trie (string_list):
	trie = Trie (string_list[0])

	for i in xrange (1, len(string_list)):
		trie.append(string_list[i])

	return trie

def match_prefix_trie (trie, text):	
	cur_node = trie.root
	index = 0
	findings = []

	while (index < len(text)):
		if text[index] in cur_node.child:
			cur_node = cur_node.child[text[index]]
			index += 1
		else:
			break

		if (len(cur_node.child) == 0):   # reached a leaf node, pattern matched, return with pattern
			findings.append (text[:index])
			break

	return findings

# returns pattern occurrences as a dictionary of key -> pattern, value -> occurrence location 0 being first letter
def exact_matching_trie (trie, text):
	pattern_locations = collections.defaultdict(list)   # dictionary of key -> pattern, value -> occurrence locations
	cur_text_start_ind = 0   # used to simulate shifting of text/pattern match window

	while (cur_text_start_ind < len(text)):
		findings = match_prefix_trie (trie, text[cur_text_start_ind:])
		for pattern_found in findings:
			pattern_locations[pattern_found].append(cur_text_start_ind)
		cur_text_start_ind += 1

	return pattern_locations

# same as that of a trie's
class Suffix_Trie_Node (object):
	# saves parent node instance, own value, and initializes an empty list of destination nodes
	def __init__(self, parent, value, node_id):
		self.value = value
		self.child = {}   # using a dict, so can search child nodes by value
		self.parent = parent
		self.node_id = node_id   # node count basically

class Suffix_Trie (object):
	# initializing suffix trie, only one string required (build up suffix list with it)
	def __init__(self, seq):
		self.root = Suffix_Trie_Node (None, None, 1)
		self.max_node_count = 2
		cur_node = self.root

		suffix_list = self.enumerate_suffix (seq)
		for suffix in suffix_list:
			# print suffix[0] + ' ' + str(suffix[1])
			self.append (suffix[0], suffix[1])  # passes both the suffix string and the index where it starts

	# saves all possible suffix strings of the sequence into list, each tuple in list represent (suffix, starting index)
	def enumerate_suffix (self, seq):
		marked_seq = seq + '$'
		suffix_list = []

		for i in xrange (len (marked_seq)):
			suffix_list.append ((marked_seq[i:], i))

		return suffix_list

	# adds the given string into the suffix trie, at '$' mark save both $ and starting index
	def append (self, suffix, starting_index):
		cur_node = self.root

		# goes down a matching path, if value does not match, start new branch
		ind = 0
		while (ind < len(suffix)):
			cur_val = suffix[ind]
			
			#look for match below
			if cur_val in cur_node.child:
				cur_node = cur_node.child[cur_val]
			else:
				if (cur_val == '$'):
					cur_val = cur_val + str(starting_index)   # simple way of saving starting index at leaf
				cur_node.child[cur_val] = Suffix_Trie_Node (cur_node, cur_val, self.max_node_count)
				cur_node = cur_node.child[cur_val]
				self.max_node_count += 1
			ind += 1

	# following DFS algorithm, checks all nodes immediate parent of leaves for longest repeat
	def longest_repeat (self):
		max_repeat_length = 0
		max_repeat_seq = ''
		stack = []    # a stack of nodes

		for key in self.root.child:
			stack.append ((self.root.child[key], self.root.child[key].value))   # push all child nodes onto stack, along with their values

		while (len(stack) > 0):
			cur_node_tuple = stack.pop ()
			cur_node = cur_node_tuple[0]
			cur_seq = cur_node_tuple[1]

			if (len(cur_node.child) >= 2):    # in the case of repeats
				if (len(cur_seq) > max_repeat_length):
					max_repeat_length = len(cur_seq)
					max_repeat_seq = cur_seq
			for key in cur_node.child:
				stack.append ((cur_node.child[key],cur_seq + cur_node.child[key].value))  # saves the extended sequence onto stack

		return max_repeat_seq

class Suffix_Tree_Node (object):
	def __init__(self,parent, value, node_id):
		# self.start_index = start_index
		# self.length = length
		self.node_id = node_id
		self.value = value
		self.child = {}
		self.parent = parent
		self.seq_occurrence = []
		self.rightmost_node = None

class Suffix_Tree (object):
	def __init__(self, seq):
		self.root = Suffix_Tree_Node(None,None,1)
		self.max_node_count = 2
		cur_node = self.root

	# naive method for suffix tree construction, requires O(n^2) run -time
	# assumes suffix list given is sorted by longest to shortest substring, format (string, starting_index)
	def build_suffix_tree_naive (self, suffix_list):
		# inserting longest substring into tree, initialization
		self.root.child[suffix_list[0][0]+str(suffix_list[0][1])] = Suffix_Tree_Node (self.root, suffix_list[0][0]+str(suffix_list[0][1]), self.max_node_count)  # starting index for substring appended at end
		self.max_node_count += 1
		for i in xrange(1, len(suffix_list)):
			branch_list = self.root.child
			suffix_str = suffix_list[i][0]
			suffix_ind = suffix_list[i][1]
			parent_node = self.root

			while (1==1):
				common_prefix = ''
				# print suffix_str + str(suffix_ind)
				for key in branch_list:
					if (suffix_str[0] == branch_list[key].value[0]):   # find which branch to take, find child node with a matching first character
						match_node = branch_list[key]
						common_prefix = os.path.commonprefix([suffix_str, match_node.value])   # find how long to take branch
						break  # from for loop when matching branch found

				if (len(common_prefix) > 0 and len(common_prefix) < len(match_node.value)):   # break current path into two, insert node where common prefix occurs
					suffix_str = suffix_str[len(common_prefix):]  # select substring after common prefix for next branch selection
					middle_node = Suffix_Tree_Node (parent_node, common_prefix, self.max_node_count)
					self.max_node_count += 1
					del parent_node.child[match_node.value]  # re-organize parent of matched node to point to new middle node as child
					parent_node.child[middle_node.value] = middle_node
					match_node.value = match_node.value[len(common_prefix):]
					match_node.parent = middle_node
					middle_node.child[match_node.value] = match_node   # add connection to broken path end
					middle_node.child[suffix_str+str(suffix_ind)] = Suffix_Tree_Node (middle_node, suffix_str+str(suffix_ind), self.max_node_count)    # add connection to new suffix string end
					self.max_node_count += 1
					# print 'path 1 , common prefix: ' + common_prefix + '\n'
					break   # from while loop
				elif (len(common_prefix) > 0):  # keeps on checking child nodes for longer prefix match
					suffix_str = suffix_str[len(common_prefix):]  # select substring after common prefix for next branch selection
					branch_list = match_node.child   # candidates for next branching point
					parent_node = match_node
					# print 'path 2' + '\n'
				else:   # no match at all
					parent_node.child[suffix_str+str(suffix_ind)] = Suffix_Tree_Node (parent_node, suffix_str+str(suffix_ind), self.max_node_count)
					self.max_node_count += 1
					# print 'path 3'
					# for key in parent_node.child:
					# 	print parent_node.child[key].value
					# print '\n'
					break

	# used when finding the longest common subsequence between multiple sequences
	# seq_mark tells function which sequence the suffixes come from
	# insert suffix into tree directly after generating the suffix
	def build_suffix_tree_multiple_seq (self, seq, seq_mark):
		if not '$' in seq:
			seq = seq + '$'
		
		for i in xrange (len (seq)):
			branch_list = self.root.child
			suffix_str = seq[i:]
			suffix_ind = i
			parent_node = self.root

			while (1==1):
				common_prefix = ''
				# print 'Current suffix: ' + suffix_str
				for key in branch_list:
					if (suffix_str[0] == branch_list[key].value[0]):   # find which branch to take, find child node with a matching first character
						match_node = branch_list[key]
						common_prefix = os.path.commonprefix([suffix_str, match_node.value])   # find how long to take branch
						break  # from for loop when matching branch found

				if (len(common_prefix) > 0 and len(common_prefix) < len(match_node.value)):   # break current path into two, insert node where common prefix occurs
					suffix_str = suffix_str[len(common_prefix):]  # select substring after common prefix for next branch selection
					middle_node = Suffix_Tree_Node (parent_node, common_prefix, self.max_node_count)
					middle_node.seq_occurrence.append (seq_mark)
					middle_node.seq_occurrence.extend (match_node.seq_occurrence)   # retaining previous sequences the matched prefix occurred in
					middle_node.seq_occurrence = list(set(middle_node.seq_occurrence))    # avoiding duplicates
					self.max_node_count += 1
					del parent_node.child[match_node.value]  # re-organize parent of matched node to point to new middle node as child
					parent_node.child[middle_node.value] = middle_node
					match_node.value = match_node.value[len(common_prefix):]
					match_node.parent = middle_node
					middle_node.child[match_node.value] = match_node   # add connection to broken path end
					temp_node = Suffix_Tree_Node (middle_node, suffix_str, self.max_node_count)    # add connection to new suffix string end
					temp_node.seq_occurrence.append (seq_mark)
					temp_node.seq_occurrence = list(set(temp_node.seq_occurrence))    # avoiding duplicates
					middle_node.child[suffix_str] = temp_node
					self.max_node_count += 1
					# print 'path 1 , common prefix: ' + common_prefix + '\n'
					break   # from while loop
				elif (len(common_prefix) > 0):  # keeps on checking child nodes for longer prefix match
					match_node.seq_occurrence.append (seq_mark)
					match_node.seq_occurrence = list(set(match_node.seq_occurrence))    # avoiding duplicates
					suffix_str = suffix_str[len(common_prefix):]  # select substring after common prefix for next branch selection
					branch_list = match_node.child   # candidates for next branching point
					parent_node = match_node
					# print 'path 2' + '\n'
					if (len(suffix_str) == 0):
						break
				else:   # no match at all
					temp_node = Suffix_Tree_Node (parent_node, suffix_str, self.max_node_count)
					temp_node.seq_occurrence.append (seq_mark)
					temp_node.seq_occurrence = list(set(temp_node.seq_occurrence))   # avoiding duplicates
					parent_node.child[suffix_str] = temp_node
					self.max_node_count += 1
					# print 'path 3'
					# for key in parent_node.child:
					# 	print parent_node.child[key].value
					# print '\n'
					break

	# returns a list of suffix tree node values (to be correct, they should be edge labels)
	# depth first traversal implemented with stack
	def retrieve_node_labels (self):
		labels = []
		stack = []    # a stack of nodes

		for key in self.root.child:
			# print self.root.child[key].value
			stack.append (self.root.child[key])   # push all child nodes onto stack

		while (len(stack) > 0):
			cur_node = stack.pop ()
			# print cur_node.value + ' : ' + str(cur_node.seq_occurrence)
			labels.append (cur_node.value)
			for key in cur_node.child:
				stack.append (cur_node.child[key])

		return labels

	# finds the longest common subsequence between multiple sequences
	# seq_mark_list contains the list of sequences to find common subsequence in 
	# suffix end marker, '$' is used but not necessary
	def longest_common_subsequence (self, seq_mark_list):
		max_shared_length = 0
		max_shared_seq = ''
		stack = []    # a stack of nodes

		for key in self.root.child:
			stack.append ((self.root.child[key], len(self.root.child[key].value)))   # push all child nodes onto stack, along with length of value

		while (len(stack) > 0):
			cur_node_tuple = stack.pop ()
			cur_node = cur_node_tuple[0]
			cur_seq_length = cur_node_tuple[1]

			common_seq = True
			# print cur_node.seq_occurrence
			for seq_mark in seq_mark_list:
				if not seq_mark in cur_node.seq_occurrence:
					common_seq = False

			if common_seq:
				if (cur_seq_length > max_shared_length):
					max_shared_length = cur_seq_length
					max_shared_node = cur_node
				for key in cur_node.child:
					stack.append ((cur_node.child[key],cur_seq_length + len(cur_node.child[key].value)))  # saves the extended sequence onto stack

		# track back to root to get longest shared subsequence
		max_shared_seq = ''
		while (max_shared_node != self.root):
			max_shared_seq = max_shared_node.value + max_shared_seq
			max_shared_node = max_shared_node.parent

		return max_shared_seq

	# returns the shortest substring from seq1 that seq2 (assuming only 2 sequences) does not contain
	# suffix end marker, '$' should not be used
	# seq_mark_from determines which sequence the uncommon substring comes from
	def shortest_uncommon_subsequence (self, seq_mark_list, seq_mark_from):
		min_nonshared_length = 10000   # initialize with a really big number
		stack = []    # a stack of nodes

		for key in self.root.child:
			stack.append ((self.root.child[key], len(self.root.child[key].value)))   # push all child nodes onto stack, along with length of value

		while (len(stack) > 0):
			cur_node_tuple = stack.pop ()
			cur_node = cur_node_tuple[0]
			cur_seq_length = cur_node_tuple[1]

			# indicates if current node includes substrings shared by both sequences
			if (len(cur_node.seq_occurrence) == 1 and (seq_mark_from in cur_node.seq_occurrence)):
				uncommon_seq = True   # found node where substring comes only from seq1
			else:
				uncommon_seq = False   # shared substring

			if uncommon_seq:
				cur_val = cur_node.value.replace('$', '')  # end marker '$' cannot be counted as a subsequence
				if (len(cur_val) > 0):  # if current node contains strings besides '$'
					cur_seq_length = cur_seq_length - len(cur_node.value) + 1   # shortest uncommon subsequence only need 1 character difference
					if (cur_seq_length < min_nonshared_length and cur_seq_length > 0):
						min_nonshared_length = cur_seq_length
						min_nonshared_node = cur_node
			else:   # still on shared subsequence, moving down a level
				for key in cur_node.child:
					stack.append ((cur_node.child[key], cur_seq_length + len(cur_node.child[key].value)))  # saves the extended sequence onto stack

		# track back to root
		min_nonshared_seq = min_nonshared_node.value[0]   # keep first character of where substring difference occurred
		min_nonshared_node = min_nonshared_node.parent
		while (min_nonshared_node != self.root):
			min_nonshared_seq = min_nonshared_node.value + min_nonshared_seq
			min_nonshared_node = min_nonshared_node.parent

		return min_nonshared_seq

	# modified from suffix trie's
	# suffix end marker, '$' is necessary	
	def longest_repeat (self):
		max_repeat_length = 0
		max_repeat_seq = ''
		stack = []    # a stack of nodes

		for key in self.root.child:
			stack.append ((self.root.child[key], self.root.child[key].value))   # push all child nodes onto stack, along with their values

		while (len(stack) > 0):
			cur_node_tuple = stack.pop ()
			cur_node = cur_node_tuple[0]
			cur_seq = cur_node_tuple[1]

			if (len(cur_node.child) >= 2):    # in the case of repeats
				if (len(cur_seq) > max_repeat_length):
					max_repeat_length = len(cur_seq)
					max_repeat_seq = cur_seq
			for key in cur_node.child:
				stack.append ((cur_node.child[key],cur_seq + cur_node.child[key].value))  # saves the extended sequence onto stack

		return max_repeat_seq

	# converts from a suffix trie to a suffix tree, one way of constructing a tree
	def trie_to_tree (self):
		stack = []    # a stack of nodes

		for key in self.root.child:
			stack.append (self.root.child[key])   # push all child nodes onto stack

		while (len(stack) > 0):
			branch_start_node = stack.pop ()
			concat_value = branch_start_node.value
			cur_node = branch_start_node

			while (len(cur_node.child) == 1):
				for only_child_val in cur_node.child:
					cur_node = cur_node.child[only_child_val]
					concat_value += cur_node.value

			# in case of branch
			for key in cur_node.child:
				stack.append (cur_node.child[key])

			# takes care of leaf and branch			
			del branch_start_node.parent.child[branch_start_node.value]   # removes node value for branch start node
			# print concat_value
			cur_node.value = concat_value
			branch_start_node.parent.child[cur_node.value] = cur_node    # add node value for current node

	# mostly identical to that of suffix trie's	
	def enumerate_suffix (self, seq):
		if not '$' in seq:
			marked_seq = seq + '$'
		else:
			marked_seq = seq
		suffix_list = []

		for i in xrange (len (marked_seq)):
			suffix_list.append ((marked_seq[i:], i))

		return suffix_list

	# mostly identical to suffix trie's
	def append (self, suffix, starting_index):
		cur_node = self.root

		# goes down a matching path, if value does not match, start new branch
		ind = 0
		while (ind < len(suffix)):
			cur_val = suffix[ind]
			
			#look for match below
			if cur_val in cur_node.child:
				cur_node = cur_node.child[cur_val]
			else:
				if (cur_val == '$'):
					cur_val = cur_val + str(starting_index)   # simple way of saving starting index at leaf
				cur_node.child[cur_val] = Suffix_Tree_Node (cur_node, cur_val, self.max_node_count)
				cur_node = cur_node.child[cur_val]
				self.max_node_count += 1
			ind += 1

class Suffix_Array (object):
	def __init__ (self):
		self.sorted_suffix_index = []

	# memory efficient, no suffix strings are stored
	def generate_suffix_array (self, seq):
		suffix_index = range(len(seq))
		self.sorted_suffix_index = sorted(suffix_index, cmp= lambda i,j: cmp (seq[i:], seq[j:]))

	# returns only parts of the suffix array, skipping all except ones that are multiples of the interval
	def generate_partial_suffix_array (self, seq, interval):
		suffix_index = range(len(seq))
		self.sorted_suffix_index = sorted(suffix_index, cmp= lambda i,j: cmp (seq[i:], seq[j:]))
		self.partial_suffix_index = {}

		for i in self.sorted_suffix_index:
			if (int(self.sorted_suffix_index[i]) % int(interval) == 0):
				self.partial_suffix_index[i] = int(self.sorted_suffix_index[i])

	# construct suffix tree from suffix array (list) and longest common prefix (list)
	# rightmost_node equals to the most recently added child of the node
	def build_suffix_tree_from_array (self, seq, suffix_array, lcp):
		suffix_tree = Suffix_Tree (seq)
		
		for i in xrange (len(lcp)):
			# print 'Current suffix : ' + seq[suffix_array[i]:]
			if (lcp[i] == 0):   # suffix branches off of root
				suffix_str = seq[suffix_array[i]:]
				temp_node = Suffix_Tree_Node (suffix_tree.root, suffix_str, suffix_tree.max_node_count)
				suffix_tree.root.child[suffix_str] = temp_node
				suffix_tree.root.rightmost_node = temp_node
				suffix_tree.max_node_count += 1	
				# print 'path 1\n'
			else:   # suffix branches somewhere in the rightmost path
				common_prefix_length = lcp[i]
				prefix_length_count = 0
				cur_node = suffix_tree.root
				parent_node = None
				while (prefix_length_count <= common_prefix_length):
					parent_node = cur_node
					cur_node = cur_node.rightmost_node
					prefix_length_count += len(cur_node.value)
					# print 'path 2 , node value: ' + cur_node.value

				if (prefix_length_count > common_prefix_length):  # case in which branch is formed by breaking the current node into 2
					common_prefix = cur_node.value[:-(prefix_length_count-common_prefix_length)]   # where breaking will take place
					middle_node = Suffix_Tree_Node (parent_node, common_prefix, suffix_tree.max_node_count)
					suffix_tree.max_node_count += 1
					del parent_node.child[cur_node.value]  # re-organize parent of current node to point to new middle node as child
					parent_node.child[middle_node.value] = middle_node
					parent_node.rightmost_node = middle_node
					cur_node.value = cur_node.value[len(common_prefix):]
					cur_node.parent = middle_node
					middle_node.child[cur_node.value] = cur_node   # add connection to broken path end
					suffix_str = seq[suffix_array[i]+lcp[i]:]  # what is left unmatched of the current suffix
					# print 'New branch value : ' + suffix_str
					temp_node = Suffix_Tree_Node (middle_node, suffix_str, suffix_tree.max_node_count)    # add connection to new suffix string end
					middle_node.child[suffix_str] = temp_node
					middle_node.rightmost_node = temp_node
					suffix_tree.max_node_count += 1
				elif (prefix_length_count == common_prefix_length):  # case in which a branch is added as the current node's child
					suffix_str = seq[suffix_array[i]+lcp[i]:]  # what is left unmatched of the current suffix					
					# print 'New branch value : ' + suffix_str
					temp_node = Suffix_Tree_Node (cur_node, suffix_str, suffix_tree.max_node_count)    # add connection to new suffix string end
					cur_node.child[suffix_str] = temp_node
					cur_node.rightmost_node = temp_node
					suffix_tree.max_node_count += 1

				# print '\n'

		return suffix_tree

	# mostly identical to that of suffix trie's	
	def enumerate_suffix (self, seq):
		if not '$' in seq:
			marked_seq = seq + '$'
		else:
			marked_seq = seq
		suffix_list = []

		for i in xrange (len (marked_seq)):
			suffix_list.append ((marked_seq[i:], i))

		return suffix_list

# returns list of cyclic rotational equivalent strings of the given sequence in lexicographical order
def enumerate_cyclic_rotations (seq):
	rotations_list = []

	# end marker need for burrows-wheeler transform
	if not '$' in seq:
		seq = seq + '$'

	for i in xrange (len(seq)):
		rotations_list.append (seq[i:] + seq[:i])

	return sorted(rotations_list)

# converts input sequence into its BWT sequence
# assumes cyclic rotations are sorted in lexicographical order
def burrows_wheeler_transform (cyclic_rotations_list):
	bwt_seq = ''

	for rotation in cyclic_rotations_list:
		bwt_seq += rotation[-1]

	return bwt_seq

# returns a list of tuples (nucleotide, occurrence_first_column)
# last column sequence is the burrows-wheeler transform
def generate_first_column (last_column_seq):
	first_column_list = []
	occurrence_counter = collections.defaultdict(int)

	first_column_seq = sorted(list(last_column_seq))
	for nucleotide in first_column_seq:
		occurrence_counter[nucleotide] += 1
		first_column_list.append ((nucleotide, occurrence_counter[nucleotide]))

	return first_column_list

# first/last column lists are lists of tuples, (nucleotide, occurrence_count)
def reconstruct_seq_bwt (first_column_list, last_column_list):
	stop_mark = last_column_list[0]
	start_mark = first_column_list[0]  # starting with '$' character, tuple ('$',1)

	reconstructed_str = ''
	cur_mark = start_mark
	while (cur_mark != stop_mark):
		reconstructed_str += cur_mark[0]  # saves only the character, not its occurrence count
		for i in xrange (len(last_column_list)):
			if (cur_mark == last_column_list[i]):
				next_mark = first_column_list[i]
				break
		cur_mark = next_mark
	reconstructed_str += stop_mark[0]  # tacking on the last character missed in the while loop
	return reconstructed_str[1:] + reconstructed_str[0]   # moving '$' end marker from first to last position

# using the data structure lists of tuples to represent character/occurrence positions for first_column and last_column of cyclic rotations
# more imaginative data structure dictionary of list?
# last column sequence is the burrows-wheeler transform
def inverse_burrows_wheeler_transform (last_column_seq):
	last_column_list = []
	occurrence_counter = collections.defaultdict(int)

	for i in xrange (len(last_column_seq)):
		nucleotide = last_column_seq[i]
		occurrence_counter[nucleotide] += 1
		last_column_list.append ((nucleotide, occurrence_counter[nucleotide]))

	first_column_list = generate_first_column (last_column_seq)
	reconstructed_str = reconstruct_seq_bwt (first_column_list, last_column_list)
	# print 'First column list : ' + '\n' + str(first_column_list)
	# print 'Last column list : ' + '\n' + str(last_column_list)

	return reconstructed_str

# creates a list with same length as first/last column holding the index of matching character in first column for the last column
# used for burrows-wheeler matching
def build_last_to_first_column_index (last_column_list, first_column_list):
	last_to_first_index = []

	for i in xrange (len(last_column_list)):
		cur_mark = last_column_list[i]
		for j in xrange (len(first_column_list)):
			if (cur_mark == first_column_list[j]):
				last_to_first_index.append (j)
				break
	return last_to_first_index

# last column sequence is the burrows-wheeler transform of original sequence
# returns frequency of matches, not match locations
def burrows_wheeler_exact_match (last_column_seq, patterns_list):
	# construct first column, with respective occurrence count
	first_column_list = generate_first_column (last_column_seq)

	# same as that in inverse_bwt function
	last_column_list = []
	occurrence_counter = collections.defaultdict(int)
	for i in xrange (len(last_column_seq)):
		nucleotide = last_column_seq[i]
		occurrence_counter[nucleotide] += 1
		last_column_list.append ((nucleotide, occurrence_counter[nucleotide]))

	# construct last to first column
	last_to_first_index = build_last_to_first_column_index (last_column_list, first_column_list)

	patterns_occurrences_list = []
	for pattern in patterns_list:
		patterns_occurrences_list.append (burrows_wheeler_match_pattern(first_column_list, last_column_list, pattern, last_to_first_index))

	return patterns_occurrences_list

# returns the number of occurrences of pattern in sequence given by its first/last column
def burrows_wheeler_match_pattern (first_column_list, last_column_list, pattern_seq, last_to_first_index):
	top = 0
	bottom = len(last_column_list) - 1

	cur_pattern = pattern_seq   # pattern will be truncated after each loop
	while (top <= bottom):
		if (len(cur_pattern) > 0):
			symbol = cur_pattern[-1]
			cur_pattern = cur_pattern[:-1]

			top_index = -1
			bottom_index = -1
			for i in xrange(top, bottom+1):
				if (last_column_list[i][0] == symbol):
					if (top_index == -1):  # first symbol appearance
						top_index = i
						bottom_index = i
					else:
						bottom_index = i

			if not (top_index == -1):
				top = last_to_first_index[top_index]
				bottom = last_to_first_index[bottom_index]
			else:
				return 0
		else:
			return bottom - top + 1

# pseudocode to burrows-wheeler match pattern, source Stepic
# BWMATCHING(FirstColumn, LastColumn, Pattern, LastToFirst)
#     top = 0
#     bottom = size(LastColumn) - 1
#     while top <= bottom
#         if Pattern is nonempty
#             symbol = last letter in Pattern
#             remove last letter from Pattern
#             if positions from top to bottom in LastColumn contain an occurrence of symbol
#                 topIndex = first position where symbol occurs among positions where symbol occurs from top to bottom in LastColumn
#                 bottomIndex = last position of symbol among positions from top to bottom in LastColumn
#                 top = LastToFirst(topIndex)
#                 bottom = LastToFirst(bottomIndex)
#             else
#                 output 0
#         else
#             output bottom - top + 1

# returns a dictionary of where each symbol first occurs in lexicographical order
def generate_first_occurrence (last_column_seq):
	first_column_list = sorted(list(last_column_seq))
	first_occurrence = {}

	for i in xrange(len(first_column_list)):
		if not first_column_list[i] in first_occurrence:   # first occurrence of symbol
			first_occurrence[first_column_list[i]] = i   # add symbol, and occurrence location

	return first_occurrence

# returns the number of occurrences of symbol up to the cutoff point
def symbol_occurrence_count (symbol, cutoff_index, last_column_seq):
	return last_column_seq[:cutoff_index].count (symbol)

# last column sequence is the burrows-wheeler transform of original sequence
# returns frequency of matches, not match locations
# different from version 1, do not need to store first column list
def burrows_wheeler_exact_match2 (last_column_seq, patterns_list):
	first_occurrence = generate_first_occurrence (last_column_seq)

	# generate list of tuples for last column symbols/occurrence count
	# same as that in inverse_bwt function
	last_column_list = []
	occurrence_counter = collections.defaultdict(int)
	for i in xrange (len(last_column_seq)):
		nucleotide = last_column_seq[i]
		occurrence_counter[nucleotide] += 1
		last_column_list.append ((nucleotide, occurrence_counter[nucleotide]))

	patterns_occurrences_list = []
	for pattern in patterns_list:
		patterns_occurrences_list.append (burrows_wheeler_match_pattern2 (first_occurrence, last_column_seq, last_column_list, pattern))

	return patterns_occurrences_list

# returns the number of occurrences of pattern in sequence given by its last column
# more space efficient/faster than version 1
def burrows_wheeler_match_pattern2 (first_occurrence, last_column_seq, last_column_list, pattern_seq):
	top = 0
	bottom = len(last_column_list) - 1

	cur_pattern = pattern_seq   # pattern will be truncated after each loop
	while (top <= bottom):
		if (len(cur_pattern) > 0):
			symbol = cur_pattern[-1]
			cur_pattern = cur_pattern[:-1]

			symbol_occurrence = False
			for i in xrange(top, bottom+1):
				if (last_column_list[i][0] == symbol):
					symbol_occurrence = True
					break

			if symbol_occurrence:
				top = first_occurrence[symbol] + symbol_occurrence_count (symbol, top, last_column_seq)
				bottom = first_occurrence[symbol] + symbol_occurrence_count (symbol, bottom+1, last_column_seq)-1
			else:
				return 0
		else:
			return bottom - top + 1

# pseudo code for bwmatch v.2, source Stepic
# BETTERBWMATCHING(FirstOccurrence, LastColumn, Pattern, Count)
#     top = 0
#     bottom = size(LastColumn) - 1
#     while top <= bottom
#         if Pattern is nonempty
#             symbol = last letter in Pattern
#             remove last letter from Pattern
#             if positions from top to bottom in LastColumn contain an occurrence of symbol
#                 top = FirstOccurrence(symbol) + Countsymbol(top, LastColumn)
#                 bottom = FirstOccurrence(symbol) + Countsymbol(bottom + 1, LastColumn) - 1
#             else
#                 output 0
#         else
#             output bottom - top + 1

# generates a list of tuples of symbols and their occurrence count
# same as the code used in burrows_wheeler_exact_match and burrows_wheeler_exact_match2
def build_last_column_list (last_column_seq):
	# generate list of tuples for last column symbols/occurrence count
	last_column_list = []
	occurrence_counter = collections.defaultdict(int)
	for i in xrange (len(last_column_seq)):
		nucleotide = last_column_seq[i]
		occurrence_counter[nucleotide] += 1
		last_column_list.append ((nucleotide, occurrence_counter[nucleotide]))

	return last_column_list

# returns the match region (in the range top to bottom) of pattern in sequence in terms of first column order (alphabetically ordered suffix)
def burrows_wheeler_match_pattern3 (first_occurrence, last_column_seq, last_column_list, pattern_seq):
	top = 0
	bottom = len(last_column_list) - 1

	cur_pattern = pattern_seq   # pattern will be truncated after each loop
	while (top <= bottom):
		if (len(cur_pattern) > 0):
			symbol = cur_pattern[-1]
			cur_pattern = cur_pattern[:-1]

			symbol_occurrence = False
			for i in xrange(top, bottom+1):
				if (last_column_list[i][0] == symbol):
					symbol_occurrence = True
					break

			if symbol_occurrence:
				top = first_occurrence[symbol] + symbol_occurrence_count (symbol, top, last_column_seq)
				bottom = first_occurrence[symbol] + symbol_occurrence_count (symbol, bottom+1, last_column_seq)-1
			else:
				return []
		else:
			return [top, bottom]

# backtracks through burrows-wheeler arrays to find the first symbol that partial suffix array contains
# obtain the original symbol location by adding distance of backtrack to current symbol location
# starting_index is where the symbol is located lexicographically/alphabetically in the first column
def locate_through_partial_suffix_array (starting_index, partial_suffix_array, last_to_first_index):
	found_in_suffix_array = False
	cur_index = int(starting_index)
	distance_from_original_index = 0   # keeps track how far is current index from starting index

	while (not found_in_suffix_array):
		# print 'Current index : ' + str(cur_index)
		if cur_index in partial_suffix_array:   # checks if index is a key in partial_suffix_array
			found_in_suffix_array = True
		else:
			distance_from_original_index += 1
			cur_index = last_to_first_index[cur_index]

	return partial_suffix_array[cur_index] + distance_from_original_index

# def approximate_match_trackforward (max_hamming_dist):

# def approximate_match_trackback (starting_index, partial_suffix_array, max_hamming_dist):
# 	found_in_suffix_array = False
# 	cur_index = int(starting_index)
# 	distance_from_original_index = 0   # keeps track how far is current index from starting index

# 	while (not found_in_suffix_array):
# 		# print 'Current index : ' + str(cur_index)
# 		if cur_index in partial_suffix_array:   # checks if index is a key in partial_suffix_array
# 			found_in_suffix_array = True
# 		else:
# 			distance_from_original_index += 1
# 			cur_index = last_to_first_index[cur_index]

# 	return partial_suffix_array[cur_index] + distance_from_original_index

# utilizes pigeon-hole principle to divide pattern into multiple chunks, which one chunk must match exactly
# input the exact match locations and which segment the matches occur
# run function for each unique pattern (query)
# requires last segment length in the case that not all segments are equal in length
def burrows_wheeler_approximate_match (text, pattern, kmer_length, max_hamming_dist, match_location_w_segment, last_seg_length):
	segments_count = max_hamming_dist+1
	approximate_match_locations = []
	for (location, which_segment) in match_location_w_segment:
		if (which_segment == 0):   # exact match at first segment, check rest of pattern against text
			hamming_dist = 0
			for i in xrange (kmer_length, len(pattern)):
				if not pattern[i] == text[location+i]:
					hamming_dist += 1
				if hamming_dist > max_hamming_dist:
					break
			if hamming_dist <= max_hamming_dist:
				approximate_match_locations.append (location)
			# print 'option 1'
		elif (which_segment == segments_count-1):  # exact match at last segment, check length of pattern beforehand
			hamming_dist = 0
			for i in xrange (len(pattern)-last_seg_length):
				if not pattern[i] == text[location-len(pattern)+last_seg_length+i]:
					hamming_dist += 1
				if hamming_dist > max_hamming_dist:
					break
			if hamming_dist <= max_hamming_dist:
				approximate_match_locations.append (location-len(pattern)+last_seg_length)
			# print 'option 2'
		else:   # match occurs somewhere in between first/last segment
			hamming_dist = 0
			for i in xrange (which_segment * kmer_length):
				if not pattern[i] == text[i-which_segment*kmer_length]:
					hamming_dist += 1
				if hamming_dist > max_hamming_dist:
					break
			for j in xrange (kmer_length + which_segment*kmer_length, len(pattern)):
				if not pattern[j] == text[j-which_segment*kmer_length]:
					hamming_dist += 1
				if hamming_dist > max_hamming_dist:
					break
			if hamming_dist <= max_hamming_dist:
				approximate_match_locations.append (location-which_segment*kmer_length)
			# print 'option 3'

	return list(set(approximate_match_locations))

# returns a list of substrings evenly dividing the given pattern into max_hamming_distance + 1 chunks
def segment_pattern (pattern, max_hamming_dist):
	segment_length = int(math.ceil(float(len(pattern))/float(max_hamming_dist+1)))

	pattern_segments = []
	segment_start_index = 0
	while (segment_start_index < len(pattern)):
		pattern_segments.append((pattern[segment_start_index:segment_start_index+segment_length], segment_start_index))
		segment_start_index += segment_length

	return pattern_segments

# returns a dictionary of kmers and their appearance locations
# does not work when patterns length vary
def build_index (text, kmer_length):
	text_index = collections.defaultdict(list)
	for i in xrange (len(text) - kmer_length + 1):
		text_index[text[i:i+kmer_length]].append(i)
	return text_index

def index_assisted_approximate_match (pattern_segments, text_index, pattern, text, kmer_length, max_hamming_dist):

	exact_match_locations = []
	approximate_match_locations = []
	which_segment = 0
	for pattern_segment in pattern_segments:
		if (len(text_index[pattern_segment[0]]) > 0):
			exact_match_locations = text_index[pattern_segment[0]]
			# print pattern_segment[0] + str(exact_match_locations)

		for location in exact_match_locations:
			if (which_segment == 0):   # exact match at first segment, check rest of pattern against text
				for location in exact_match_locations:
					hamming_dist = 0
					for i in xrange (kmer_length, len(pattern)):
						if not pattern[i] == text[location+i]:
							hamming_dist += 1
						if hamming_dist > max_hamming_dist:
							break
					if hamming_dist <= max_hamming_dist:
						approximate_match_locations.append (location)
				# print 'option 1'
			elif (which_segment == len(pattern_segments)-1):  # exact match at last segment, check length of pattern beforehand
				for location in exact_match_locations:
					hamming_dist = 0
					for i in xrange (len(pattern)-kmer_length):
						if not pattern[i] == text[location-len(pattern)+kmer_length+i]:
							hamming_dist += 1
						if hamming_dist > max_hamming_dist:
							break
					if hamming_dist <= max_hamming_dist:
						approximate_match_locations.append (location-len(pattern)+kmer_length)
				# print 'option 2'
			else:   # match occurs somewhere in between first/last segment
				for location in exact_match_locations:
					hamming_dist = 0
					for i in xrange (which_segment * kmer_length):
						if not pattern[i] == text[i-which_segment*kmer_length]:
							hamming_dist += 1
						if hamming_dist > max_hamming_dist:
							break
					for j in xrange (kmer_length + which_segment*kmer_length, len(pattern)):
						if not pattern[j] == text[j-which_segment*kmer_length]:
							hamming_dist += 1
						if hamming_dist > max_hamming_dist:
							break
					if hamming_dist <= max_hamming_dist:
						approximate_match_locations.append (location-which_segment*kmer_length)
				# print 'option 3'
		which_segment += 1

	return list(set(approximate_match_locations))	

def index_assisted_approximate_match2 (pattern_segments, pattern, text, kmer_length, max_hamming_dist):

	exact_match_locations = []
	approximate_match_locations = []
	for pattern_segment in pattern_segments:
		segment_str = pattern_segment[0]
		segment_start_index = pattern_segment[1]
		segment_end_index = segment_start_index + len(segment_str)
		exact_match_locations = find_all_overlap (text, segment_str)

		for location in exact_match_locations:
			hamming_dist = 0
			for i in xrange (0, segment_start_index):
				if not pattern[i] == text[location-segment_start_index+i]:
					hamming_dist += 1
				if hamming_dist > max_hamming_dist:
					break
			for j in xrange (segment_end_index, len(pattern)):
				if not pattern[j] == text[location-segment_start_index+j]:
					hamming_dist += 1
				if hamming_dist > max_hamming_dist:
					break
			if hamming_dist <= max_hamming_dist:
				approximate_match_locations.append (location-segment_start_index)

	return list(set(approximate_match_locations))

# returns all occurrences of substring in text
def find_all_overlap (text, substring):
    start_index = 0
    occurrence_locations = []

    while 1==1:
    	start_index = text.find(substring, start_index)
    	if start_index == -1: 
    		return occurrence_locations
    	else:
    		occurrence_locations.append(start_index)

        start_index += 1



