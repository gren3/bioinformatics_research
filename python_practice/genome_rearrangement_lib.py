#!/usr/bin/env python

import sys
import argparse
import collections   #defaultdict
from collections import Counter
from operator import add
import itertools
import numpy
import string

def greedy_reversal_sort (seq_init):
    reversal_distance = 0
    cur_seq = seq_init

    for i in xrange (1, len(seq_init)+1):   
        if (int(cur_seq[i-1][1:]) != i):    # checks if element i-1 in list is the number (+)i, if not find and swap
            reversal_distance += 1
            # print 'Position ' + str(i) + ' with value ' + cur_seq[i-1][1:] + ' is not sorted'
            for j in xrange (i, len(seq_init)+1):
                if (int(cur_seq[j][1:]) == i):
                    partial_reversed_seq = cur_seq[i-1:j+1][::-1]    
                    # sign reversal after number reversal
                    for k in xrange(len(partial_reversed_seq)):
                        if (partial_reversed_seq[k][0] == '+'):
                            partial_reversed_seq[k] = '-' + partial_reversed_seq[k][1:]
                        else:
                            partial_reversed_seq[k] = '+' + partial_reversed_seq[k][1:]
                    # print 'partially reversed sequence : ' + str(partial_reversed_seq)
                    cur_seq = cur_seq[:i-1] + partial_reversed_seq + cur_seq[j+1:]
                    break
            # print 'Current sequence : ' + str(cur_seq)
            print '(' + (' ').join(cur_seq) + ')'   # printing with a certain format
        if (cur_seq[i-1][0] != '+'):    # check if sign is +, if not, take a step to re-orient
            reversal_distance += 1
            cur_seq[i-1] = '+' + cur_seq[i-1][1:]
            print '(' + (' ').join(cur_seq) + ')'   # printing with a certain format
    return

def count_breakpoint (seq_init):
    breakpoints = 0
    position_marker = 1
    sign_init = seq_init[0][0]   # starting sign, either (+) or (-)
    number_init = int(seq_init[0][1:])   # starting number
    # if first element of sequence is not +1, count up breakpoint
    if (sign_init != '+' or number_init != 1):
        breakpoints += 1
    prev_sign = sign_init
    prev_number = number_init

    while (position_marker < len(seq_init)):
        cur_sign = seq_init[position_marker][0]
        cur_number = int(seq_init[position_marker][1:])
        if (cur_sign == prev_sign):
            if (cur_sign == '+' and cur_number != prev_number+1):
                breakpoints += 1
            elif (cur_sign == '-' and cur_number != prev_number-1):
                breakpoints += 1
        else:
            breakpoints += 1
        prev_sign = cur_sign
        prev_number = cur_number
        position_marker += 1

    # check if last element is the largest, with the right sign
    if (int(seq_init[position_marker-1][1:]) != position_marker or seq_init[position_marker-1][0] != '+'):
        breakpoints += 1

    return breakpoints

# taken from process_dna.py
def reverse_complement (dna_seq):
    complement = string.maketrans('ACTG','TGAC')
    return string.translate(dna_seq, complement)[::-1]

def synteny_block (seq1, seq2, kmer_length):
    seq1_kmer_location_index = collections.defaultdict(list)
    seq2_kmer_location_index = collections.defaultdict(list)

    # populate sequence 1 kmer dictionary
    for i in xrange (len(seq1) - kmer_length + 1):
        seq1_kmer_location_index[seq1[i:i+kmer_length]].append (i)   # adding the index of occurrence into list

    # populate sequence 2 kmer dictionary, taking into account reverse complement sequence as well
    seq2_revcomp = reverse_complement (seq2)
    for j in xrange (len(seq2) - kmer_length + 1):
        seq2_kmer_location_index[seq2[j:j+kmer_length]].append (j)
    for k in xrange (len(seq2_revcomp) - kmer_length + 1):
        seq2_kmer_location_index[seq2_revcomp[k:k+kmer_length]].append (len(seq2_revcomp) - k - kmer_length)   # since it's reverse complement, take original sequence's index

    # iterate and print the matched kmers in both seq1 and seq2 and their respective indices
    for kmer in seq1_kmer_location_index:
        if (len(seq2_kmer_location_index[kmer]) > 0):   # if seq2 contains 1 or more occurrences of current kmer
            seq1_occurrences = list(set(seq1_kmer_location_index[kmer]))
            seq2_occurrences = list(set(seq2_kmer_location_index[kmer]))
            for seq1_occurrence in seq1_occurrences:
                for seq2_occurrence in seq2_occurrences:
                    print '(' + str(seq1_occurrence) + ', ' + str(seq2_occurrence) + ')'
    return

# converts (+/-) numbered elements into graph edges
def process_alignment (alignment_list):
    edges = []

    for i in xrange(len(alignment_list)):
        if (i == len(alignment_list)-1):   # last element's pair requires wrap around to first element
            if (alignment_list[0][0] == '+'):
                cur_edge = ((alignment_list[i][1:]+alignment_list[i][0], alignment_list[0][1:]+'-'))
            else:
                cur_edge = ((alignment_list[i][1:]+alignment_list[i][0], alignment_list[0][1:]+'+'))
        else:
            if (alignment_list[i+1][0] == '+'):
                cur_edge = ((alignment_list[i][1:]+alignment_list[i][0], alignment_list[i+1][1:]+'-'))
            else:
                cur_edge = ((alignment_list[i][1:]+alignment_list[i][0], alignment_list[i+1][1:]+'+'))
        edges.append (cur_edge)
    return edges

def trace_graph_cycles (graph_edges):
    edge_dict_1 = collections.defaultdict (list)
    edge_dict_2 = collections.defaultdict (list)

    # read in edges, convert from tuples to dictionary, since undirected create 2 dictionaries going both ways
    for (v1, v2) in graph_edges:
        edge_dict_1[v1].append (v2)
        edge_dict_2[v2].append (v1)

    cycle_counter = 0

    start_edge = graph_edges[0]
    cur_edge = start_edge
    start_node = start_edge[0]
    # print graph_edges
    # print edge_dict_1
    # print edge_dict_2
    while len(graph_edges) > 0:
        # print 'Current edge: ' + str(cur_edge)
        if cur_edge[1] in edge_dict_1[cur_edge[0]]:
            # print 'Edge dict 1 sink nodes: ' + str(edge_dict_1[cur_edge[0]])
            edge_dict_1[cur_edge[0]].remove(cur_edge[1])
            edge_dict_2[cur_edge[1]].remove(cur_edge[0])    # since graph is undirected need to remove edge considering both directions
        elif cur_edge[1] in edge_dict_2[cur_edge[0]]:
            # print 'Edge dict 2 sink nodes: ' + str(edge_dict_2[cur_edge[0]])
            edge_dict_2[cur_edge[0]].remove(cur_edge[1])
            edge_dict_1[cur_edge[1]].remove(cur_edge[0])   # since graph is undirected need to remove edge considering both directions

        if cur_edge in graph_edges:
            graph_edges.remove (cur_edge)
        else:
            graph_edges.remove ((cur_edge[1],cur_edge[0]))

        sink_list = edge_dict_1[cur_edge[1]][:]   # making a copy, avoid modifying the actual list
        sink_list.extend (edge_dict_2[cur_edge[1]])
        # print sink_list
        # dict_1_list = edge_dict_2['-2']
        # print dict_1_list
        if (len(sink_list) > 0):
            next_edge = (cur_edge[1], sink_list[0])

        cur_edge = next_edge
        if (cur_edge[1] == start_node):
            cycle_counter += 1
            # print 'cycle completed'
            if cur_edge in graph_edges:
                graph_edges.remove (cur_edge)
            else:
                graph_edges.remove ((cur_edge[1],cur_edge[0]))
            if (len(graph_edges) > 0):
                cur_edge = graph_edges[0]
                start_node = cur_edge[0]
        
    return cycle_counter




