#!/usr/bin/env bash

# javac ProcessDNA.java
# java ProcessDNA

# javac ParallelProcessDNA.java
# java ParallelProcessDNA 2 4 1

# python parallel_dna_process_mapper.py --input_path 'mapper_input.txt' --hamm_bound 2 --kmer_length 10 --command 'most_freq_kmer_w_mismatch' >mapper_output.txt
# cat mapper_output.txt | sort | python parallel_dna_process_reducer.py 

# python preprocess_dna.py --input_path 'data.txt' --kmer_length 10 --num_mappers 3 >mapper_input.txt 
# cat mapper_input.txt | python parallel_dna_process_mapper_nargs.py | sort | python parallel_dna_process_reducer.py

# python parallel_motif_process_mapper.py --input_path 'motif_mapper_input.txt' --command 'greedy_motif_selection' --kmer_length 12 >motif_mapper_output.txt
# cat motif_mapper_output.txt | sort | python parallel_motif_process_reducer.py

# re-writing training dna sequences into mapper input format
# python preprocess_classify.py --input_path 'enh_fb.fa' --output_path 'positive_sample.txt' --starting_sample_location 1 --training_sample_size 10

# finds the most frequent k-mer motifs with mismatch and output its dna profile 
# python parallel_motif_process_mapper.py --input_path 'positive_sample.txt' --command 'greedy_motif_selection' --kmer_length 12 >motif_mapper_output.txt
# if more than 1 mapper is used, reduce all profiles by average each corresponding base probabilities
# cat motif_mapper_output.txt | sort | python parallel_motif_process_reducer.py
# creating test sequences from positive and negative enhancer sequences
# python preprocess_classify.py --input_path 'enh_fb.fa' --output_path 'classify_test_seqs/pos_sample.txt' --starting_sample_location 11 --training_sample_size 10
# python preprocess_classify.py --input_path 'nullseqsi_200_1.fa' --output_path 'classify_test_seqs/neg_sample.txt' --starting_sample_location 1 --training_sample_size 10

# use test dna sequences to check if motif profile is accurate for enhancer sequences
# cat classify_test_seqs/* > 'classify_input.txt'
python main_classify.py --input_path 'classify_input.txt' --profile_path 'motif_mapper_output.txt' --label_path 'labels.txt' --kmer_length 12




