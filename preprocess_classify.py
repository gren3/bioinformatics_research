#!/usr/bin/env python

import sys
import argparse
import string
import time
import random
import numpy


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description="Preprocess training data")
	parser.add_argument('--input_path', metavar='path', type=str, help='Input file path')
	parser.add_argument('--output_path', metavar='path', type=str, help='Output file path')
	parser.add_argument('--training_sample_size', metavar='int', type=int, help='Number of sample to retrieve from file')
	parser.add_argument('--starting_sample_location', metavar='int', type=int, help='From which sample to start with')
	args = parser.parse_args()
	
	output_fh = open (args.output_path, 'w')
	sample_count = 1
	read_count = 1
	temp_seq = ''
	# rewrites the training data into a format which the mapper takes in
	with open(args.input_path, 'r') as input_fh:
		input_fh.readline() # read off initial title of dna seq.
		while True:
			input_str = input_fh.readline()
			if len(input_str) == 0: break
			input_str = input_str.strip().upper()

			if '>' in input_str:
				if (sample_count >= args.starting_sample_location):
					if (read_count <= args.training_sample_size):  #using two-thirds data for training
						output_fh.write (temp_seq + '\n')
					read_count += 1
				sample_count += 1
			else:
				temp_seq = input_str

